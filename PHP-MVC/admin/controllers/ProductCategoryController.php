<?php 
 require MODEL_PATH . 'ProductCategory.php';
class ProductCategoryController {

    protected $productCategoryModel;

     public function __construct()
     {
          $this->productCategoryModel = new ProductCategory();
     }

     public function index()
     {
          $data = [];
          $product_categories = $this->productCategoryModel->getProductCategories();
          $data['product_categories'] = $product_categories;

          return view('product_categories.index',$data);

     }

     public function isnert()
     {
          return view('product_categories.create');
     }

     public function update()
     {
          return view('product_categories.update');
     }

     public function delete()
     {
           
     }
}
