<?php 
 require MODEL_PATH . 'Brand.php';
class BrandController {

    protected $BrandModel;

     public function __construct()
     {
          $this->BrandModel = new Brand();
     }

     public function index() 
     {
          $data = [];
          $brands = $this->BrandModel->getBrands();
          $data['brands'] = $brands;

          return view('brands.index',$data);

     }

     public function insert()
     { 
          $insert = $this->BrandModel->insertBrand($name,$slug,$imgae,$description);
          return view('brands.insert');
     }

     public function update()
     {
          return view('brands.update');
     }

     public function delete()
     {
           
     }
}
