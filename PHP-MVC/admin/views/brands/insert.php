<?php 
   $err = [];
           $name = '';
           $slug = '';
           $image = '';
           $description = ''; 
   if (isset($_POST['sub'])) {
        if (!isset($_POST['name']) || $_POST['name'] == "") {
             $err[] = "Fill the name";
        }

        if (!isset($_POST['slug']) || $_POST['slug'] == "") {
             $err[] = "Fill the slug";
        }

        if (!isset($_POST['image']) || $_POST['image'] == "") {
             $err[] = "Fill the image";
        }

        if (!isset($_POST['description']) || $_POST['description'] == "") {
             $err[] = "Fill the description";
        }
           
           $name = $_POST['name'];
           $slug = $_POST['slug'];
           $image = $_POST['image'];
           $description = $_POST['description'];
 
   }
    // require MODEL_PATH .'Brand.php';
 ?>
<!DOCTYPE html>
<html>
<head>
	<title>Add Brand</title>
</head>
<body>
     <?php  
         if (isset($_POST['sub'])) {
            if (count($err) > 0) {
               for ($i=0; $i < count($err); $i++) { ?>
               <p style="color: red"><?php echo $err[$i] ?></p>     
     <?php     }
            }

            else {
               header('Location: index.php?c=Brand&m=index');
            }
         }   
     ?>
     <form method="POST" action="">
     <table border="1px">
     	  <tr>
     	  	 <td>Name</td>
     	  	 <td>Slug</td>
     	  	 <td>Image</td>
     	  	 <td>Description</td>
     	  </tr>
     	  <tr>
                 <td><input type="" name="name"></td>
                 <td><input type="" name="slug"></td> 
                 <td><input type="" name="image"></td>
                 <td><input type="" name="description"></td>    
            </tr>  
     </table>	
     <br>
    <input type="submit" name="sub">
    </form>
</body>
</html>