<?php  
       $err = [];
       session_start(); 
      if (!isset($_SESSION['user'])) {
         header("Location: sinh_vien_login.php");
       } 
       require('connect.php');
       // Lấy dữ liệu
       $masv = $_GET['ma_sv'];
       $sql = "SELECT * FROM sinh_vien WHERE ma_sv = '{$masv}' LIMIT 1";
       $query = $db->query($sql);
       $sv = $query->fetch_assoc();
       if (is_null($sv)) {
       	 header("Location: sinh_vien.php");
       }
       if (isset($_POST['sub'])) {
         

         //Kiem tra :
           if (!isset($_POST['ma_sv']) || $_POST['ma_sv'] == "") {
                $err[] = "Mã sinh viên bị trống ! <br>";
           }
           if (!isset($_POST['ho_ten']) || $_POST['ho_ten'] == "") {
                $err[] = "Họ tên sinh viên bị trống ! <br>";
           }
           if (!isset($_POST['ngay_sinh']) || $_POST['ngay_sinh'] == "") {
                $err[] = "Ngày sinh trống ! <br>";
           }
           if (!isset($_POST['gioi_tinh']) || $_POST['gioi_tinh'] == "") {
                $err[] = "Hãy chọn giới tính ! <br>";
           }
           if (!isset($_POST['email']) || $_POST['email'] == "") {
                $err[] = "Email sinh viên bị trống ! <br>";
           }
           if (!isset($_POST['dia_chi']) || $_POST['dia_chi'] == "") {
                $err[] = "Địa chỉ sinh viên bị trống ! <br>";
           }
           if (!isset($_POST['ma_khoa']) || $_POST['ma_khoa'] == "") {
                $err[] = "Hãy chọn khoa ! <br>";
           }

          if (count($err) == 0){
            
               $ma_sv = trim($_POST['ma_sv']);
               $ho_ten = trim($_POST['ho_ten']);
               $ngay_sinh = trim($_POST['ngay_sinh']);
               $gioi_tinh = trim($_POST['gioi_tinh']);
               $email = trim($_POST['email']);
               $dia_chi = trim($_POST['dia_chi']);
               $ma_khoa = trim($_POST['ma_khoa']);
            $sql = "UPDATE sinh_vien SET ho_ten = '". $ho_ten ."', ngay_sinh = '". $ngay_sinh ."', gioi_tinh = '". $gioi_tinh ."', email = '". $email ."', dia_chi = '". $dia_chi ."', ma_khoa = '". $ma_khoa ."' WHERE ma_sv = '". $ma_sv ."'" ;
            $query = $db->query($sql);

            if (!$query) {
                $err[] = "Không thể sửa, thao tác bị lỗi";
            }
         }
       }
?>
<!DOCTYPE html>
<html>
<head>
	<title>Sửa Sinh Viên</title>

	<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="css/fontawesome-all.min.css">
    <link rel="stylesheet" type="text/css" href="css/owl.carousel.css">
   <link rel="yylesheet" type="text/css" href="css/owl.theme.default.min.css">
  <link rel="stylesheet" type="text/css" href="css/style_trangchu.css">
</head>
<body>
  
      <div class="container">
      	 <div class="row head">
      	 	 <div class="col-md-2 logo">
      	 	 	<img src="img/logo.png">
      	 	 </div>
      	 	 <div class="col-md-10 tieude">
      	 	 	<center><h3>Sửa Sinh Viên</h3></center>
      	 	 </div>
      	 </div>
 
      	<div class="menu">
      		<ul class="row">
      			<li class="col-md-3">
      				<center><a href="trang_chu.php">Trang chủ</a></center>
      			</li>
              
      			<li class="col-md-3">
      				<center><a href="sinh_vien.php?page_now=0">Sinh viên</a></center>
      			</li>

      			<li class="col-md-3">
      				<center><a href="khoa.php">Khoa</a></center>
      			</li>

      			<li class="col-md-3">
      				<center><a href="mon.php">Môn học</a></center>
      			</li>
      		</ul>


      	</div> 
      	<form action="" method="POST">
          
         <?php  
            if (isset($_POST['sub'])) {

          
              if (count($err) > 0) {
                 for ($i=0; $i < count($err) ; $i++) { 
                      echo $err[$i];
                 }
              }

              else echo "Thành Công";

               } 
         ?>
             <h3>Sửa</h3>
             <table border="1px">
             	   <tr>
             	   	  <td>
             	   	  	Mã Sinh Viên
             	   	  </td>
             	   	  <td>
             	   	  	 Tên Sinh Viên Mới
             	   	  </td>
                    <td>
                       Ngày Sinh Mới
                    </td>
                    <td>
                       Giới Tính Mới(Nam/Nữ)
                    </td>
                    <td>
                       Email Mới
                    </td>
                    <td>
                       Địa Chỉ Mới
                    </td>
                    <td>
                       Chọn Khoa
                    </td>
                    <td>
                        Trạng Thái
                    </td>
             	   </tr>
             	   <tr>
             	   	  <td>
             	   	    <input readonly="readonly" type="text" name="ma_sv" value=" <?php 
                                  if(isset($_POST['ma_sv'])) {
                                  	  echo($_POST['ma_sv']);
                                  }
                                  else
                                  	 echo($sv['ma_sv']);
             	   	     ?> ">
             	   	  </td>
                    <td>
                       <input type="text" name="ho_ten" value =" <?php  
                                if (isset($_POST['ho_ten'])) {
                                      echo   $_POST['ho_ten'];
                                }
                                else echo $sv['ho_ten'];
                      ?>">
                    </td>
                    <td>
                       <input type="text" name="ngay_sinh" value =" <?php  
                                if (isset($_POST['ngay_sinh'])) {
                                      echo   $_POST['ngay_sinh'];
                                }
                                else echo $sv['ngay_sinh'];
                      ?>">
                    </td>
                    <td>
                       <select name="gioi_tinh">
                            <option value="">-Chọn-</option>
                            <option value="1"
                                  <?php 
                                        
                                           if (isset($_POST['gioi_tinh']) && $_POST['gioi_tinh'] == '1') {
                                             echo "selected";
                                           }
                                        
                                        else {
                                        	if ($sv['gioi_tinh'] == '1') {
                                        		 echo "selected";
                                        	}
                                        }
                                   ?>
                             >Nam</option>
                            <option value="0"
                                  <?php 
                                       
                                           if (isset($_POST['gioi_tinh']) && $_POST['gioi_tinh'] == '0') {
                                             echo "selected";
                                           }
                                        
                                        else {
                                        	if ($sv['gioi_tinh'] == '0') {
                                        		 echo "selected";
                                        	}
                                        }
                                   ?>
                            >Nữ</option>
                       </select>
                    </td>
                    <td>
                        <input type="text" name="email" value =" <?php  
                                if (isset($_POST['email'])) {
                                      echo   $_POST['email'];
                                }
                                else echo $sv['email'];
                      ?>">
                    </td>
                    <td>
                        <input type="text" name="dia_chi" value =" <?php  
                                if (isset($_POST['dia_chi'])) {
                                      echo   $_POST['dia_chi'];
                                }
                                else echo $sv['dia_chi'];
                      ?>">
                    </td>
                    <td>
                        <select name="ma_khoa">
                             <option value="">-Chọn-</option>
                             <?php  
                                  $sql = "SELECT * FROM khoa";
                                  $query = $db->query($sql);
                                  $result = $query->fetch_all(MYSQLI_ASSOC);
                                  if (count($result) > 0) {
                                    foreach ($result as $khoa) { ?>
                                  <option value="<?php echo $khoa['ma_khoa'] ?>" <?php 
                                           if ((isset($_POST['ma_khoa']) && $_POST['ma_khoa'] == $khoa['ma_khoa']) || ($sv['ma_khoa'] == $khoa['ma_khoa'])) {
                                             echo "selected";
                                           }

                                   ?> >
                                   	  <?php 
                                          echo $khoa['ten_khoa'];
                                   	   ?>
                                   </option>

                                 <?php 
                                     }
                                  }
                                  ?> 
                        </select>
                    </td>
                    <td>
                         <select name="status">
                            <option value="">-Chọn-</option>
                            <option value="1"
                                  <?php                                        
                                           if (isset($_POST['status']) && $_POST['status'] == '1') {
                                             echo "selected";
                                           } else {
                                                if ($sv['status'] == '1') {
                                                   echo "selected";
                                                }
                                           }
                                   ?>
                             >Enable</option>
                            <option value="0"
                                  <?php                                 
                                        if (isset($_POST['status']) && $_POST['status'] == '0') {
                                             echo "selected";
                                           } else {
                                                if ($sv['status'] == '0') {
                                                   echo "selected";
                                                }
                                        }
                                   ?>
                            >Disable</option>
                       </select>
                    </td>
             	   </tr>
             </table>
            <br>
            <br>
            
             <p>
                <input type="submit" name="sub" value="OK">
             </p>
         </form>


       </div>

     

    <script src="js/jquery-3.2.1.min.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script>	
</body>
</html>