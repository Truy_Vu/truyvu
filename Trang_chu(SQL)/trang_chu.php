<?php  
       require('connect.php');
       session_start();
       if (!isset($_SESSION['user'])) {
         header("Location: sinh_vien_login.php");
       }
?>
<!DOCTYPE html>
<html>
<head>
	<title>Sinh viên</title>

	<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="css/fontawesome-all.min.css">
    <link rel="stylesheet" type="text/css" href="css/owl.carousel.css">
   <link rel="yylesheet" type="text/css" href="css/owl.theme.default.min.css">
  <link rel="stylesheet" type="text/css" href="css/style_trangchu.css">
</head>
<body>
  
      <div class="container">
      	 <div class="row head">
      	 	 <div class="col-md-2 logo">
      	 	 	<img src="img/logo.png">
      	 	 </div>
      	 	 <div class="col-md-10 tieude">
      	 	 	<center><h3>Trang chủ</h3></center>
      	 	 </div>
      	 </div>
 
      	<div class="menu">
      		<ul class="row">
      			<li class="col-md-3">
      				<center><a href="trang_chu.php">Trang chủ</a></center>
      			</li>
              
      			<li class="col-md-3">
      				<center><a href="sinh_vien.php?page_now=0">Sinh viên</a></center>
      			</li>

      			<li class="col-md-3">
      				<center><a href="khoa.php">Khoa</a></center>
      			</li>

      			<li class="col-md-3">
      				<center><a href="mon.php">Môn học</a></center>
      			</li>
      		</ul>
      	</div> 
        <a href="logout.php">Đăng xuất</a>
        <?php  
             if ($_SESSION['user']['type'] == "admin") {
        ?>
        <p>
           <a href="themdiem.php">Thêm điểm</a>
        </p>
        <p>
           <a href="danh_sach.php">Danh sách điểm</a>
        </p>
        <?php 
           }
         ?>
      
        <p>
          <b>
          	Tổng số  
        	<?php $sql = "SELECT COUNT(*) FROM khoa";
            $query = $db->query($sql);
            $result = $query->fetch_row();
            echo $result[0];
           
        	?>
             khoa bao gồm : 
            <?php 
               $sql = "SELECT * FROM khoa";
               $query = $db->query($sql);
               $result = $query->fetch_all(MYSQLI_ASSOC);
               if (count($result) > 0) {
                 foreach ($result as $sv) {
                 	echo $sv['ten_khoa'] . " , ";
                 }
               }
            ?>
          </b>	

        </p>
         
        <p>
        	<b>
        		Các môn giảng dạy : 
        		<?php
                  $sql = "SELECT *FROM mon_hoc";
                  $query = $db->query($sql);
                  $result = $query->fetch_all(MYSQLI_ASSOC);
                  if (count($result) >0) {
                  	 foreach ($result as $mon) {
                  	 	echo $mon['ten_mh']. " , ";
                  	 }
                  }
        		?>
        	</b>
        </p>

        <p>
        	<b>
        		Tổng sô sinh viên theo học :
        		<?php
                   $sql = "SELECT COUNT(*) FROM sinh_vien";
                   $query = $db->query($sql);
                   $result = $query->fetch_row();
                   echo $result[0];
                   $db->close();
        		?>
        	</b>
        </p>
       </div>


      </div>
     

    <script src="js/jquery-3.2.1.min.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script>	
</body>
</html>