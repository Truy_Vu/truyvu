<?php  
       require('connect.php');
       session_start();
       if (!isset($_SESSION['user'])) {
         header("Location: sinh_vien_login.php");
       }

      $dk = "";
       $ma_mh = "";
       $ten_mh = "";
       $err = [];
       if (isset($_POST['sub'])) {
           if ((!isset($_POST['ma_mh']) || $_POST['ma_mh'] == "") && (!isset($_POST['ten_mh']) || $_POST['ten_mh'] == "")) {
                $err[] = "Không có dữ liệu tìm kiếm<br>";
           }
           else {
              if (isset($_POST['ma_mh']) && $_POST['ma_mh'] != "") {
                   $ma_mh = "ma_mh = '" . $_POST['ma_mh'] . "'";
                    $dk = $ma_mh;
              }             
             
              if (isset($_POST['ten_mh']) && $_POST['ten_mh'] != "") {
                   $ten_mh = "ten_mh = '" . $_POST['ten_mh'] . "'";
                          if (strlen($dk) == 0) {
                      $dk = $ten_mh;
                    }  else {
                      $dk = $dk . "," . $ten_mh;
                    }
              }
             

           }    
       }
?>
<!DOCTYPE html>
<html>
<head>
  <title>Môn học</title>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="css/fontawesome-all.min.css">
    <link rel="stylesheet" type="text/css" href="css/owl.carousel.css">
   <link rel="yylesheet" type="text/css" href="css/owl.theme.default.min.css">
  <link rel="stylesheet" type="text/css" href="css/style_trangchu.css">
</head>
<body>
    
      <div class="container">
         <div class="row head">
           <div class="col-md-2 logo">
            <img src="img/logo.png">
           </div>
           <div class="col-md-10 tieude">
            <center><h3>Môn học</h3></center>
           </div>
         </div>

        <div class="menu">
          <ul class="row">
            <li class="col-md-3">
              <center><a href="trang_chu.php">Trang chủ</a></center>
            </li>
              
            <li class="col-md-3">
              <center><a href="sinh_vien.php?page_now=0">Sinh viên</a></center>
            </li>

            <li class="col-md-3">
              <center><a href="khoa.php">Khoa</a></center>
            </li>

            <li class="col-md-3">
              <center><a href="mon.php">Môn học</a></center>
            </li>
          </ul>
        </div>        
     
     <a href="logout.php">Đăng xuất</a>  
    <div class="row">    
    <div class="col-md-6">
      <?php 
            if ($_SESSION['user']['type'] == "admin") {
       ?>
      <p>
         <a href="mon_add.php">Thêm môn mới</a>
     </p>
     <?php } ?>
      <p>
          <b>
           Danh sách Môn
          </b>  
     
        </p>
    <center>
          <table border="1px" class="tex-center">
            <tr>
                <td>
                  STT
                </td>
                <td>
                  Mã Môn
                </td>
                <td>
                  Tên Môn
                </td>             
            </tr>
            <?php  

                   $sql = "SELECT * 
                    FROM mon_hoc";
                   $query= $db->query($sql);
                   $result = $query->fetch_all(MYSQLI_ASSOC);

                   if (count($result) > 0) {
                       $i = 0;
                      foreach ($result as $mon_hoc) {
                        $i++; ?>
                         <tr>
                           <td> <?php echo $i; ?> </td>
                           <td> <?php echo $mon_hoc['ma_mh']; ?> </td>
                           <td> <?php echo $mon_hoc['ten_mh']; ?></td>
                            <?php 
                                  if ($_SESSION['user']['type'] == "admin") {
                             ?>
                           <td> <a href="mon_edit.php?ma_mh=<?php echo $mon_hoc['ma_mh']?>">Sửa</a></td>
                           <td> <a href="mon_delete.php?ma_mh=<?php echo $mon_hoc['ma_mh']?>">Xóa</a></td>
                           <?php } ?>
                         </tr>
            <?php      
                      }
                   }
            ?>
        </table>
    </center>
    
    </div>
    <div  class="col-md-6">
       <?php 
      if ($_SESSION['user']['type'] == "admin") {
      ?>
      <p>
            <a href="mon_sort.php">Sắp Xếp</a>
          </p>
      <?php } ?>
    <form action="" method="POST">
          
         <h3>Tìm Kiếm</h3>
         <br>
         <br>
         <?php 
               

          ?>
         <p>
             Mã Môn : 
             <input type="text" name="ma_mh"
                 <?php  
                    if (isset($_POST['sub'])) {
                        echo "value = '" . $_POST['ma_mh'] . "'";
                    }
                 ?>
             >
         </p>
         <p>
             Tên Môn :
             <input type="text" name="ten_mh" 
                 <?php  
                    if (isset($_POST['sub'])) {
                        echo "value = '" . $_POST['ten_mh'] . "'";
                    }
                 ?>
              >
         </p>
         <p>
             <input type="submit" name="sub" value="OK">
         </p>
      </form>

        <table border="1px">
             
             <?php  
                if (isset($_POST['sub'])) {  ?>
                 <tr>
                 <td>
                     Mã Môn
                 </td>
                 <td>
                     Tên Môn
                 </td>

             </tr>
                <?php
                     if (count($err) > 0) {
                        for ($i=0; $i < count($err) ; $i++) { 
                          echo $err[$i];
                        }
                     }
                
               else {

                $sql = "SELECT * FROM mon_hoc WHERE " . $dk;
                $query = $db->query($sql);
                  if ($query) {            
                      $result = $query->fetch_all(MYSQLI_ASSOC);
                      if (count($result) >0) {     
                        foreach ($result as $khoa) {
                           echo "<tr>";
                           echo "<td>" . $khoa['ma_mh'] ."</td>";
                           echo "<td>" . $khoa['ten_mh'] . "</td>";
                           echo "</tr>";
                        }
                      } 
                     else echo "Không tìm thấy";  
                   } else {
                    echo "Không thể tìm thấy";
                   }   
                }
               } 
             ?>
        </table>   
  </div>
   </div>
        
       </div>


     

    <script src="js/jquery-3.2.1.min.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script> 
</body>
</html>