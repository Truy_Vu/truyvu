<?php  
       require('connect.php');
       session_start();
       if (!isset($_SESSION['user'])) {
         header("Location: sinh_vien_login.php");
       }
       $dk = "";
       $ma_khoa = "";
       $ten_khoa = "";
       $err = [];
       if (isset($_POST['sub'])) {
           if ((!isset($_POST['ma_khoa']) || $_POST['ma_khoa'] == "") && (!isset($_POST['ten_khoa']) || $_POST['ten_khoa'] == "")) {
                $err[] = "Không có dữ liệu tìm kiếm<br>";
           }
           else {
              if (isset($_POST['ma_khoa']) && $_POST['ma_khoa'] != "") {
                   $ma_khoa = "ma_khoa LIKE '%" . $_POST['ma_khoa'] . "%'";
                    $dk = $ma_khoa;
              }             
             
              if (isset($_POST['ten_khoa']) && $_POST['ten_khoa'] != "") {
                   $ten_khoa = "ten_khoa LIKE '%" . $_POST['ten_khoa'] . "%'";
                          if (strlen($dk) == 0) {
                      $dk = $ten_khoa;
                    }  else {
                      $dk = $dk . " AND " . $ten_khoa;
                    }
              }
             

           }    
       }

?>
<!DOCTYPE html>
<html>
<head>
  <title>Khoa</title>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="css/fontawesome-all.min.css">
    <link rel="stylesheet" type="text/css" href="css/owl.carousel.css">
   <link rel="yylesheet" type="text/css" href="css/owl.theme.default.min.css">
  <link rel="stylesheet" type="text/css" href="css/style_trangchu.css">
</head>
<body>
    
      <div class="container">
         <div class="row head">
           <div class="col-md-2 logo">
            <img src="img/logo.png">
           </div>
           <div class="col-md-10 tieude">
            <center><h3>Khoa</h3></center>
           </div>
         </div>

        <div class="menu">
          <ul class="row">
            <li class="col-md-3">
              <center><a href="trang_chu.php">Trang chủ</a></center>
            </li>
              
            <li class="col-md-3">
              <center><a href="sinh_vien.php?page_now=0">Sinh viên</a></center>
            </li>

            <li class="col-md-3">
              <center><a href="khoa.php">Khoa</a></center>
            </li>

            <li class="col-md-3">
              <center><a href="mon.php">Môn học</a></center>
            </li>
          </ul>
        </div>        
       <a href="logout.php">Đăng xuất</a>
    <div class="row">    
    <div class="col-md-6">
    <?php 
           if ($_SESSION['user']['type'] == "admin") {
     ?>  
    <p>
      <a href="khoa_add.php">Thêm mới khoa</a>
    </p>  
    <?php } ?>
    <center>
       <p>
          <b>
           Danh sách Khoa
          </b>  

        </p>
          <table border="1px" class="tex-center">
            <tr>
                <td>
                  STT
                </td>
                <td>
                  Mã Khoa
                </td>
                <td>
                  Tên Khoa
                </td>             
            </tr>
            <?php  

                   $sql = "SELECT *
                    FROM khoa";
                   $query= $db->query($sql);
                   $result = $query->fetch_all(MYSQLI_ASSOC);

                   if (count($result) > 0) {
                       $i = 0;
                      foreach ($result as $khoa) {
                        $i++; ?>
                          <tr>
                          <td> <?php echo $i; ?> </td>
                          <td><?php echo $khoa['ma_khoa']; ?></td>
                          <td><?php echo $khoa['ten_khoa']; ?></td>
                          <?php  if ($_SESSION['user']['type'] == "admin") { ?>
                          <td><a href="khoa_edit.php?ma_khoa=<?php echo $khoa['ma_khoa'] ?>">Sửa</a></td>
                          <td><a href="khoa_delete.php?ma_khoa=<?php echo $khoa['ma_khoa']?>">Xóa</a></td>
                          </tr>
                          <?php } ?>
               <?php
                      }
                   }
            ?>
        </table>
    </center>
    
    </div>
    <div class="col-md-6 tex-center">
      <?php if ($_SESSION['user']['type'] == "admin") {?>
       <p>
           <a href="khoa_sort.php">Sắp xếp</a> 
       </p>
       <?php } ?>
        <center><p>
          <b>
            Tổng sinh viên mỗi khoa :    

          </b>
        </p>
          <table border="1px">
                <tr>
                    <td>
                       <b>
                         Tên Khoa
                       </b>
                    </td>
                    <td>
                         <b>
                           Số sinh viên
                         </b>
                    </td>
                </tr>

                 <?php  
                     $sql = "SELECT khoa.ten_khoa , COUNT(sinh_vien.ma_sv) AS nums 
                          FROM khoa
                          LEFT JOIN sinh_vien
                          ON khoa.ma_khoa = sinh_vien.ma_khoa
                          GROUP BY khoa.ma_khoa";
                     $query = $db->query($sql);
                     $result = $query->fetch_all(MYSQLI_ASSOC);
                    if (count($result) > 0) {
                          foreach ($result as $khoa) {
                            echo "<tr>";
                            echo "<td>" . $khoa['ten_khoa'] . "</td>";
                            echo "<td>" . $khoa['nums'] . "</td>";
                            echo "</tr>";
                          }
                       }   

                 ?>
          </table>
        </center>
    </div>
   </div>
      <br>
      <br>
  <div >
    <form action="" method="POST">
         <h3>Tìm Kiếm</h3>
         <br>
         <br>
         <?php 
               

          ?>
         <p>
             Mã Khoa : 
             <input type="text" name="ma_khoa"
                 <?php  
                    if (isset($_POST['ma_khoa'])) {
                        echo "value = '" . $_POST['ma_khoa'] . "'";
                    }
                 ?>
             >
         </p>
         <p>
             Tên Khoa :
             <input type="text" name="ten_khoa" 
                 <?php  
                    if (isset($_POST['ten_khoa'])) {
                        echo "value = '" . $_POST['ten_khoa'] . "'";
                    }
                 ?>
              >
         </p>
         <p>
             <input type="submit" name="sub" value="OK">
         </p>
      </form>

        <table border="1px">
            
             <?php  
                if (isset($_POST['sub'])) { ?> 
                     <tr>
                 <td>
                     Mã Khoa
                 </td>
                 <td>
                     Tên Khoa
                 </td>

             </tr>

                <?php
                     if (count($err) > 0) {
                        for ($i=0; $i < count($err) ; $i++) { 
                          echo $err[$i];
                        }
                     }
                
               else {

                $sql = "SELECT * FROM khoa WHERE " . $dk;
                $query = $db->query($sql);
                  if ($query) {            
                      $result = $query->fetch_all(MYSQLI_ASSOC);
                      if (count($result) >0) {     
                        foreach ($result as $khoa) {
                           echo "<tr>";
                           echo "<td>" . $khoa['ma_khoa'] ."</td>";
                           echo "<td>" . $khoa['ten_khoa'] . "</td>";
                           echo "</tr>";
                        }
                      } 
                     else echo "Không tìm thấy";  
                   } else {
                    echo "Không thể tìm thấy";
                   }   
                }
               } 
             ?>
        </table>   
  </div>

       </div>


     

    <script src="js/jquery-3.2.1.min.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script> 
</body>
</html>