<?php  
       require('connect.php');

?>
<!DOCTYPE html>
<html>
<head>
	<title>Danh sách điểm</title>

	<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="css/fontawesome-all.min.css">
    <link rel="stylesheet" type="text/css" href="css/owl.carousel.css">
   <link rel="yylesheet" type="text/css" href="css/owl.theme.default.min.css">
  <link rel="stylesheet" type="text/css" href="css/style_trangchu.css">
</head>
<body>
  
      <div class="container">
      	 <div class="row head">
      	 	 <div class="col-md-2 logo">
      	 	 	<img src="img/logo.png">
      	 	 </div>
      	 	 <div class="col-md-10 tieude">
      	 	 	<center><h3>Danh sách điểm</h3></center>
      	 	 </div>
      	 </div>
 
      	<div class="menu">
      		<ul class="row">
      			<li class="col-md-3">
      				<center><a href="trang_chu.php">Trang chủ</a></center>
      			</li>
              
      			<li class="col-md-3">
      				<center><a href="sinh_vien.php?page_now=0">Sinh viên</a></center>
      			</li>

      			<li class="col-md-3">
      				<center><a href="khoa.php">Khoa</a></center>
      			</li>

      			<li class="col-md-3">
      				<center><a href="mon.php">Môn học</a></center>
      			</li>
      		</ul>
      	</div> 
       

        <h3>Danh sách điểm của sinh viên</h3>
        <br><br>
         <center>
               <table border="1px">
                    <tr>
                       <td>
                         Họ và Tên
                       </td>
                       <td>
                         Tên Môn Học
                       </td>
                       <td>
                         Điểm
                       </td>
                    </tr>
                      <?php

                            $sql = "SELECT sinh_vien.ho_ten, mon_hoc.ten_mh, ket_qua.diem                 
                            FROM sinh_vien 
                            INNER JOIN ket_qua 
                            ON sinh_vien.ma_sv = ket_qua.ma_sv
                            INNER JOIN mon_hoc
                            ON ket_qua.ma_mh = mon_hoc.ma_mh";
                            $query = $db->query($sql);
                            $result = $query->fetch_all(MYSQLI_ASSOC);
                            if (count($result) > 0) {
                               foreach ($result as $sv_diem) {
                                 echo "<tr>";
                                 echo "<td>". $sv_diem['ho_ten'] . "</td>";
                                 echo "<td>". $sv_diem['ten_mh'] . "</td>";
                                 echo "<td>". $sv_diem['diem'] . "</td>";
                                 echo "</tr>";
                               }
                            }
                       ?>     
               </table>

         </center>
       </div>


    <script src="js/jquery-3.2.1.min.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script>	
</body>
</html>