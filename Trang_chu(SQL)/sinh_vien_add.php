<?php  
      $err = []; 
      $ds_khoa = [];
       require('connect.php');
       session_start(); 
      if (!isset($_SESSION['user'])) {
         header("Location: sinh_vien_login.php");
       } 
       if (isset($_POST['sub'])) {
          if (!isset($_POST['ma_sv']) || $_POST['ma_sv'] == '') {
              $err[] = 'Mã sinh viên không được bỏ trống<br>';
          }
          if (!isset($_POST['ho_ten']) || $_POST['ho_ten'] == '') {
              $err[] = 'Họ tên không được bỏ trống<br>';
          }
          if (!isset($_POST['ngay_sinh']) || $_POST['ngay_sinh'] == '') {
              $err[] = 'Ngày sinh không được bỏ trống<br>';
          }
          if (!isset($_POST['gioi_tinh']) || $_POST['gioi_tinh'] == '') {
              $err[] = 'Giới tính không được bỏ trống<br>';
          }
          if (!isset($_POST['dia_chi']) || $_POST['dia_chi'] == '') {
              $err[] = 'Địa chỉ không được bỏ trống<br>';
          }
          if (!isset($_POST['email']) || $_POST['email'] == '') {
              $err[] = 'Email không được bỏ trống<br>';
          }
          if (!isset($_POST['ma_khoa']) || $_POST['ma_khoa'] == '') {
              $err[] = 'Hãy chọn khoa<br>';
          }
          $ma = trim($_POST['ma_sv']);
          $sql = "SELECT * FROM sinh_vien WHERE ma_sv = '" . $ma . "'";
          $query = $db->query($sql);
          $result = $query->fetch_all(MYSQLI_ASSOC);
           if (count($result) > 0) {
                $err[] = "Mã sinh viên đã tồn tại<br>";
             }
             if (count($err) == 0) {
                 $ma_sv = trim($_POST['ma_sv']);
                 $ho_ten = trim($_POST['ho_ten']);
                 $ngay_sinh = trim($_POST['ngay_sinh']);
                 $gioi_tinh = trim($_POST['gioi_tinh']);
                 $dia_chi = trim($_POST['dia_chi']);
                 $email = trim($_POST['email']);
                 $ma_khoa = trim($_POST['ma_khoa']);
                 echo $ma_sv. "," . $ho_ten. "," . $ngay_sinh. "," . $gioi_tinh. "," . $dia_chi. "," . $email. "," . $ma_khoa;
                 $sql = "INSERT INTO sinh_vien(ma_sv, ho_ten, ngay_sinh, gioi_tinh, email, dia_chi, ma_khoa ) VALUES ( ' " . $ma_sv. "','". $ho_ten . "','". $ngay_sinh . "','" .$gioi_tinh . "','" . $email . "', '" . $dia_chi . "','" . $ma_khoa . "')";
                 $query = $db->query($sql);
             }
       }
      
?>
<!DOCTYPE html>
<html>
<head>
	<title>Thêm Sinh viên</title>

	<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="css/fontawesome-all.min.css">
    <link rel="stylesheet" type="text/css" href="css/owl.carousel.css">
   <link rel="yylesheet" type="text/css" href="css/owl.theme.default.min.css">
  <link rel="stylesheet" type="text/css" href="css/style_trangchu.css">
</head>
<body>
      <div class="container">
      	 <div class="row head">
      	 	 <div class="col-md-2 logo">
      	 	 	<img src="img/logo.png">
      	 	 </div>
      	 	 <div class="col-md-10 tieude">
      	 	 	<center><h3>Thêm Sinh Viên</h3></center>
      	 	 </div>
      	 </div>

      	<div class="menu">
      		<ul class="row">
      			<li class="col-md-3">
      				<center><a href="trang_chu.php">Trang chủ</a></center>
      			</li>
              
      			<li class="col-md-3">
      				<center><a href="sinh_vien.php?page_now=0">Sinh viên</a></center>
      			</li>

      			<li class="col-md-3">
      				<center><a href="khoa.php">Khoa</a></center>
      			</li>

      			<li class="col-md-3">
      				<center><a href="mon.php">Môn học</a></center>
      			</li>
      		</ul>
      	</div>       	
       
        <h3>Thêm Sinh Viên</h3>

         <?php  
            if (isset($_POST['sub'])) {
            	 if (count($err) > 0) {
		                 for ($i=0; $i < count($err); $i++) { 
		                 	  echo $err[$i];
		                 }
	             }
	             else {
	                if ($query) {
	                		echo "Thành công";
	                } 
	                else {
	                	    echo "Không thể thêm, thao tác bị lỗi";
	                }
	             }

            }
         ?>

        <form action="" method="POST">
        	 <p>
         	Mã Sinh Viên :
         	<input type="text" name="ma_sv"  <?php if (isset($_POST['sub'])) {
                      echo "value = ".$_POST['ma_sv'];
                  } ?>  >
         </p>
         <p>
         	Tên Sinh Viên :
         	<input type="text" name="ho_ten"  <?php if (isset($_POST['sub'])) {
                      echo "value = ".$_POST['ho_ten'];
                  } ?>>
         </p>
         <p>
         	Ngày sinh :
         	<input type="text" name="ngay_sinh"  <?php if (isset($_POST['sub'])) {
                      echo "value = ".$_POST['ngay_sinh'];
                  } ?>>
         </p>
         <p>
         	Giới tính : 
         	<input type="radio" name="gioi_tinh" value="1" <?php 
               if (isset($_POST['sub'])) {
                   if ($_POST['gioi_tinh'] == 1) {
                      echo "checked";
                   }
               }
           ?> > Nam
         	<input type="radio" name="gioi_tinh" value="0" <?php 
               if (isset($_POST['sub'])) {
                   if ($_POST['gioi_tinh'] != 1) {
                      echo "checked";
                   }
               }
           ?>> Nữ
         </p>
         <p>
         	Địa chỉ :
         	<input type="text" name="dia_chi"  <?php if (isset($_POST['sub'])) {
                      echo "value = ".$_POST['dia_chi'];
                  } ?>>
         </p>
         <p>
         	Email :
         	<input type="text" name="email"  <?php if (isset($_POST['sub'])) {
                      echo "value = ".$_POST['email'];
                  } ?>>
         </p>
          <p>
         	Khoa : 
         	<select name = "ma_khoa">
         	   <option name="ma_khoa" value="">-- Chọn --</option>
         	   <?php  
                   $sql = "SELECT * FROM khoa";
                   $query = $db->query($sql);
                   $result = $query->fetch_all(MYSQLI_ASSOC);
                   if (count($result) > 0) {
                   	  foreach ($result as $ds_khoa) {
                   	  	 echo "<option  value = '" . $ds_khoa['ma_khoa']. "'>". $ds_khoa['ten_khoa'] ."</option>";
                   	  }
                   }
         	   ?>
         </select>
         </p>
         <p>
         	<input type="submit" name="sub" value="OKE">
         </p>

        </form>
      </div>
        

    <script src="js/jquery-3.2.1.min.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script>	
</body>
</html>