<?php  
      require('connect.php');
      session_start();
      $err = [];
      if (isset($_POST['submit'])) {
          if (!isset($_POST['username']) || trim($_POST['username']) == "") {
              $err[] = "Tên tài khoản bị bỏ trống<br>";
          }

          if (!isset($_POST['pass']) || trim($_POST['pass']) == "") {
              $err[] = "Mật khẩu bị bỏ trống<br>";
          }
       if (count($err) == 0) {
          $username = trim($_POST['username']);
          $pass = (string)trim($_POST['pass']);
          $sql =  "SELECT * FROM sinh_vien WHERE ma_sv = '". $username ."' LIMIT 1";
          $query = $db->query($sql);
          $result = $query->fetch_assoc();
          if (count($result) == 0) {
             $err[] = "Sinh viên không có trong dữ liệu<br>";
          } else {
             if ($result['status'] == '0') {
                 $err[] = "Tài khoản bị ngừng hoạt động<br>";
             }


             if ($result['pass'] != $pass) {
                 $err[] = "Mật khẩu sai<br>";
             }
          }
           if (count($err) == 0) {
              $_SESSION['user'] = ['username' => $username, 'pass' => $pass, 'type' => "student" ];
           }   
      } 
     } 
      
?>
<!DOCTYPE html>
<html>
<head>
	<title>Đăng nhập </title>

	<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="css/fontawesome-all.min.css">
    <link rel="stylesheet" type="text/css" href="css/owl.carousel.css">
   <link rel="yylesheet" type="text/css" href="css/owl.theme.default.min.css">
  <link rel="stylesheet" type="text/css" href="css/style_trangchu.css">
</head>
<body>
  
      <div class="container">
      	 <div class="row head">
      	 	 <div class="col-md-2 logo">
      	 	 	<img src="img/logo.png">
      	 	 </div>
      	 	 <div class="col-md-10 tieude">
      	 	 	<center><h3>Đăng nhập </h3></center>
      	 	 </div>
      	 </div>
 
      	<div class="menu">
      		<ul class="row">
      			<li class="col-md-3">
      				<center><a href="trang_chu.php">Trang chủ</a></center>
      			</li>
              
      			<li class="col-md-3">
      				<center><a href="sinh_vien.php?page_now=0">Sinh viên</a></center>
      			</li>

      			<li class="col-md-3">
      				<center><a href="khoa.php">Khoa</a></center>
      			</li>

      			<li class="col-md-3">
      				<center><a href="mon.php">Môn học</a></center>
      			</li>
      		</ul>


      	</div> 
          
      <br>
      <br>
       <?php 

                 if (isset($_POST['submit'])) {
                      if (count($err) > 0) {
                           for ($i=0; $i < count($err); $i++) { 
                               echo $err[$i];
                           }
                      }
                      else {
                         $_SESSION['flag_mess'] = "Thành công";
                         header('Location:trang_chu.php');
                      }
                 }
        ?>
      <form action="" method="POST">
            <p>
               User :  <input type="text" name="username" value="<?php 
                      if(isset($_POST['username'])) {
                        echo $_POST['username'];
                      }
                ?>">
            </p>

            <p>
               Password : 
                       <input type="password" name="pass">               
            </p>
            <p>
                 <input type="submit" name="submit" value="Login as stundent" >
            </p>
            <p>
              <i class="far fa-hand-point-right"></i>  <a href="admin_login.php">Admin Login</a>
            </p>
      </form>
       </div>

     

    <script src="js/jquery-3.2.1.min.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script>	
</body>
</html>