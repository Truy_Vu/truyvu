<?php  
session_start(); 
      if (!isset($_SESSION['user'])) {
         header("Location: sinh_vien_login.php");
       } 
      require('connect.php');
      $err = [];
      if (isset($_POST['sub'])) {
          if(!isset($_POST['criteria']) || $_POST['criteria'] == "") {
               $err[] = "Chưa nhập tiêu chí !<br>";
          }

           if(!isset($_POST['way']) || $_POST['way'] == "") {
               $err[] = "Chưa nhập cách sắp xếp !<br>";
          }
          
          if (count($err) == 0) {
             $dk = $_POST['criteria'] . " " . $_POST['way'];
             $sql= "SELECT * FROM sinh_vien ORDER BY ". $dk ."
                ";
                $query = $db->query($sql);
                $result = $query->fetch_all(MYSQLI_ASSOC);
              }
      }
?>
<!DOCTYPE html>
<html>
<head>
	<title>Xóa Sinh Viên</title>

	<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="css/fontawesome-all.min.css">
    <link rel="stylesheet" type="text/css" href="css/owl.carousel.css">
   <link rel="yylesheet" type="text/css" href="css/owl.theme.default.min.css">
  <link rel="stylesheet" type="text/css" href="css/style_trangchu.css">
</head>
<body>
  
      <div class="container">
      	 <div class="row head">
      	 	 <div class="col-md-2 logo">
      	 	 	<img src="img/logo.png">
      	 	 </div>
      	 	 <div class="col-md-10 tieude">
      	 	 	<center><h3>Xóa Sinh Viên</h3></center>
      	 	 </div>
      	 </div>
 
      	<div class="menu">
      		<ul class="row">
      			<li class="col-md-3">
      				<center><a href="trang_chu.php">Trang chủ</a></center>
      			</li>
              
      			<li class="col-md-3">
      				<center><a href="sinh_vien.php?page_now=0">Sinh viên</a></center>
      			</li>

      			<li class="col-md-3">
      				<center><a href="khoa.php">Khoa</a></center>
      			</li>

      			<li class="col-md-3">
      				<center><a href="mon.php">Môn học</a></center>
      			</li>
      		</ul>


      	</div> 
          
      <br>
      <br>
       <form action="" method="POST">
           <h3>Chọn Tiêu Chí</h3>
           <p>
              <input type="radio" name="criteria" value="ma_sv" <?php 
                       if (isset($_POST['criteria'])) {
                            if ($_POST['criteria'] == 'ma_sv') {
                                echo "checked";
                            }
                       }
                   ?>> Mã Sinh Viên
                  
           </p>
           <p>
              <input type="radio" name="criteria" value="ho_ten"<?php 
                       if (isset($_POST['criteria'])) {
                            if ($_POST['criteria'] == 'ho_ten') {
                                echo "checked";
                            }
                       }
                   ?>> Họ Tên
           </p>
           <p>
             
           <p>
              <input type="radio" name="criteria" value="ma_khoa"<?php 
                       if (isset($_POST['criteria'])) {
                            if ($_POST['criteria'] == 'ma_khoa') {
                                echo "checked";
                            }
                       }
                   ?>> Mã Khoa
           </p>
           <p>  
           </p>
           <h3>Chọn Các Sắp Xếp</h3>
           <p> 
              <input type="radio" name="way" value="ASC" <?php 
                       if (isset($_POST['way'])) {
                            if ($_POST['way'] == 'ASC') {
                                echo "checked";
                            }
                       }
                   ?>> Tăng Dần
           </p>
           <p>   
              <input type="radio" name="way" value="DESC" <?php 
                       if (isset($_POST['way'])) {
                            if ($_POST['way'] == 'DESC') {
                                echo "checked";
                            }
                       }
                   ?>> Giảm Dần
           </p>
           <p>
               <input type="submit" name="sub" value="Sắp Xếp">
           </p>
       </form>


        <?php  
            if (isset($_POST['sub']))  {
              
               if (count($err) > 0) {
                  for ($i=0; $i < count($err); $i++) { 
                     echo $err[$i];
                  }
               }
               if (count($err) == 0) { ?>
             <table border="1px">
                <tr>
                    <td>
                      Mã Sinh Viên
                    </td>
                    <td>
                       Họ Tên
                    </td>

                    <td>
                       Ngày Sinh
                    </td>

                    <td>
                      Giới tính
                    </td>

                    <td>
                      Mã Khoa
                    </td>
                </tr>
           <?php                 
                foreach ($result as $sinh_vien) { ?>
                  <tr>
                      <td> <?php echo $sinh_vien['ma_sv']; ?> </td>
                      <td> <?php echo $sinh_vien['ho_ten']; ?> </td>
                      <td> <?php echo $sinh_vien['ngay_sinh']; ?> </td>
                      <td> <?php if ($sinh_vien['gioi_tinh'] == '1') {
                      	 echo "Nam";
                      } 
                           else {
                           	  echo "Nữ";
                           }
                      ?> </td>
                      <td> <?php echo $sinh_vien['ma_khoa']; ?> </td>
                  </tr>
                        
              <?php  } ?>       
             </table  >
           <?php      
               }
            }   
           ?>

       </div>

     

    <script src="js/jquery-3.2.1.min.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script>	
</body>
</html>