<?php  
      $err = []; 
       require('connect.php');
       session_start(); 
      if (!isset($_SESSION['user'])) {
         header("Location: sinh_vien_login.php");
       } 
       if (isset($_POST['sub'])) {
          if (!isset($_POST['ma_mh']) || !isset($_POST['ten_mh']) ||$_POST['ma_mh'] == '' || $_POST['ten_mh'] == '') {
           $err[] = 'Mã môn hoặc tên môn bị bỏ trống';
          }
        $ma = trim($_POST['ma_mh']);
        $sql = "SELECT ma_mh FROM mon_hoc WHERE ma_mh = '".$ma."'";
        $query = $db->query($sql);
        $result = $query->fetch_all(MYSQLI_ASSOC);
        if (count($result) >0) {
         $err[] = 'Mã môn đã tồn tại';
        }
         if (count($err) == 0) {
         	 $ma_mh = $_POST['ma_mh'];
             $ten_mh = $_POST['ten_mh'];
             $sql = "INSERT INTO mon_hoc (ma_mh, ten_mh) VALUES('".$ma_mh."','".$ten_mh."')";
             $query = $db->query($sql);
            
         }
       }
      
?>
<!DOCTYPE html>
<html>
<head>
	<title>Thêm Môn</title>

	<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="css/fontawesome-all.min.css">
    <link rel="stylesheet" type="text/css" href="css/owl.carousel.css">
   <link rel="yylesheet" type="text/css" href="css/owl.theme.default.min.css">
  <link rel="stylesheet" type="text/css" href="css/style_trangchu.css">
</head>
<body>
      <div class="container">
      	 <div class="row head">
      	 	 <div class="col-md-2 logo">
      	 	 	<img src="img/logo.png">
      	 	 </div>
      	 	 <div class="col-md-10 tieude">
      	 	 	<center><h3>Thêm Môn Học</h3></center>
      	 	 </div>
      	 </div>

      	<div class="menu">
      		<ul class="row">
      			<li class="col-md-3">
      				<center><a href="trang_chu.php">Trang chủ</a></center>
      			</li>
              
      			<li class="col-md-3">
      				<center><a href="sinh_vien.php?page_now=0">Sinh viên</a></center>
      			</li>

      			<li class="col-md-3">
      				<center><a href="khoa.php">Khoa</a></center>
      			</li>

      			<li class="col-md-3">
      				<center><a href="mon.php">Môn học</a></center>
      			</li>
      		</ul>
      	</div>       	
       
        <h3>Thêm Môn</h3>
        <form action="" method="POST">
        	 <p>
         	Mã Môn :
         	<input type="text" name="ma_mh">
         </p>
         <p>
         	Tên Môn :
         	<input type="text" name="ten_mh">
         </p>
         <p>
         	<input type="submit" name="sub" value="OKE">
         </p>
        </form>
      </div>
         <?php  
            if (isset($_POST['sub'])) {
            	 if (count($err) > 0) {
		                 for ($i=0; $i < count($err); $i++) { 
		                 	  echo $err[$i];
		                 }
	             }
	             else {
	                if ($query) {
	                		echo "Thành công";
	                } 
	                else {
	                	    echo "Không thể thêm, thao tác bị lỗi";
	                }
	             }

            }
         ?>

    <script src="js/jquery-3.2.1.min.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script>	
</body>
</html>