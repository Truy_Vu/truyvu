<?php  
       require('connect.php');
       session_start();
       if (!isset($_SESSION['user'])) {
         header("Location: sinh_vien_login.php");
       }

       $dk = "";
       $ma_sv1 = "";
       $ho_ten1 = "";
       $err = [];
       if (isset($_POST['sub'])) {
        // Lấy thông tin
            if ((!isset($_POST['ma_sv']) || trim($_POST['ma_sv']) == "") && (!isset($_POST['ho_ten']) || trim($_POST['ho_ten'])) =="") {
                  $err[] = "Không có sữ liệu tìm kiếm";            
                }
             else {
              if (isset($_POST['ma_sv']) && trim($_POST['ma_sv']) != "") {
                   $ma_sv1 = "ma_sv LIKE '%" . trim($_POST['ma_sv']) . "%'";
                    $dk = $ma_sv1;
              }             
             
              if (isset($_POST['ho_ten']) && trim($_POST['ho_ten']) != "") {
                   $ho_ten1 = "ho_ten LIKE '%" . (trim($_POST['ho_ten'])) . "'%";
                          if (strlen($dk) == 0) {
                      $dk = $ho_ten1;
                    }  else {
                      $dk = $dk . " AND " . $ho_ten1;
                    }
              }
             }
        echo $dk ;
       }
?>
<!DOCTYPE html>
<html>
<head>
  <title>Sinh Viên</title>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="css/fontawesome-all.min.css">
  <link rel="stylesheet" type="text/css" href="css/owl.carousel.css">
  <link rel="stylesheet" type="text/css" href="css/owl.theme.default.min.css">
  <link rel="stylesheet" type="text/css" href="css/style_trangchu.css"> 
</head>
<body>
    
      <div class="container">
         <div class="row head">
           <div class="col-md-2 logo">
            <img src="img/logo.png">
           </div>
           <div class="col-md-10 tieude">
            <center><h3>Sinh Viên</h3></center>
           </div>
         </div>

        <div class="menu">
          <ul class="row">
            <li class="col-md-3">
              <center><a href="trang_chu.php">Trang chủ</a></center>
            </li>
              
            <li class="col-md-3">
              <center><a href="sinh_vien.php?page_now=0">Sinh viên</a></center>
            </li>

            <li class="col-md-3">
              <center><a href="khoa.php">Khoa</a></center>
            </li>

            <li class="col-md-3">
              <center><a href="mon.php">Môn học</a></center>
            </li>
          </ul>
        </div>  

       <a href="logout.php">Đăng xuất</a>
    <div class="row">    
    <div class="col-md-8">
      <?php if ($_SESSION['user']['type'] == "admin") {
      ?>
      <p>
          <a href="sinh_vien_add.php">Thêm mới sinh viên</a>
        </p>
     <?php } ?>   
         <p>
          <b>
           Danh sách sinh viên
          </b>  
    
        </p>
    <center>
          <table border="1px" class="tex-center">
            <tr>
                <td>
                  STT
                </td>
                <td>
                  Mã SV
                </td>
                <td>
                  Họ và tên
                </td>
                <td>
                  Ngày Sinh(yyyy/mm/dd)
                </td>
                <td>
                  Giới tính
                </td>
                <td>
                  Khoa
                </td>
            </tr>
            <?php  
                   $sql = "SELECT count(*) FROM sinh_vien";
                   $query = $db->query($sql);
                   $result = $query->fetch_row();
                   $total_pages = (int)($result[0]/10) +1;
                   $page_now = $_GET['page_now'];
                   $sql = "SELECT sinh_vien.ma_sv,sinh_vien.ho_ten,sinh_vien.ngay_sinh,sinh_vien.gioi_tinh,khoa.ten_khoa
                    FROM sinh_vien
                    INNER JOIN khoa
                    ON khoa.ma_khoa = sinh_vien.ma_khoa 
                    LIMIT 10
                    OFFSET ". $page_now ."
                    ";
                   $query= $db->query($sql);
                   $result = $query->fetch_all(MYSQLI_ASSOC);

                   if (count($result) > 0) {
                       $i = 0;
                      foreach ($result as $sv) {
                        $i++; ?>
                        <tr>
                          <td><?php  echo $i; ?> </td>
                          <td> <?php echo $sv['ma_sv']; ?> </td>
                          <td><?php  echo $sv['ho_ten']; ?> </td>
                          <td> <?php echo $sv['ngay_sinh']; ?> </td>
                          <td><?php if ($sv['gioi_tinh'] == '1') {
                              echo "Nam"; 
                            
                          } 
                          else {
                              echo "Nữ";
                            }?> </td>
                          <td> <?php echo $sv['ten_khoa']; ?> </td>

                          <?php if ($_SESSION['user']['type'] == "admin") { ?>
                          <td> <a href="sinh_vien_edit.php?ma_sv=<?php echo $sv['ma_sv']; ?>" 
                            >Sửa</a> </td>
                          <td> <a href="sinh_vien_delete.php?ma_sv=<?php 
                            echo $sv['ma_sv'];  
                           ?>">Xóa</a></td>
                           <?php } ?>
                       </tr>

              <?php        }
                   }
            ?>                    
        </table>
                       <br>
                       <br>  
               
                 <div class="pick">
                   <center>
                      <ul >
                          <li> <?php if ($page_now == 0) {?>
                             <b>Back</b> <?php } else { ?>
                            <a href="sinh_vien.php?page_now=<?php echo($page_now-10) ?>">Back</a>
                            <?php } ?>
                          </li>
                          <?php 
                             for ($i=0; $i < $total_pages; $i++) {
                           ?>
                     <li><a <?php if ((int)($page_now/10) == $i) { echo "style = 'color :red'";} ?> href="sinh_vien.php?page_now=<?php echo($i*10) ?>"> <?php echo $i+1; ?> </a></li>
                           <?php  }?> 
                          <li> <?php if ((int)($page_now/10) == $total_pages-1) {?>
                             <b>Next</b> <?php } else { ?>
                            <a href="sinh_vien.php?page_now=<?php echo($page_now+10) ?>">Next</a>
                            <?php } ?>
                          </li>
                      </ul>
                   </center>   
                 </div> 
                
    </center>
    
    </div>
    <div class="col-md-4">
      <?php 
                if ($_SESSION['user']['type'] == "admin") {
       ?>
      <p>
         <a href="sinh_vien_sort.php">Sắp xếp</a>
      </p>
      <?php 
      } ?>
        <p>
          <b>
            Tổng số nam : 

            <?php
               $sql = "SELECT COUNT(*) FROM sinh_vien 
               WHERE  gioi_tinh = 1";
               $query = $db->query($sql);
               $result = $query->fetch_row();
               echo $result[0];
            ?>
          </b>
        </p>

        <p>
          <b>
             Tổng số nữ :
             <?php
               $sql = "SELECT COUNT(*) FROM sinh_vien 
               WHERE  gioi_tinh <> 1";
               $query = $db->query($sql);
               $result = $query->fetch_row();
               echo $result[0];
            ?>
          </b>
        </p>

            <div >
    <form action="" method="POST">
         <h3>Tìm Kiếm</h3>
         <br>
         <br>
         <?php 
               

          ?>
         <p>
             Mã Sinh Viên : 
              <input type="text" name="ma_sv" value="<?php  if(isset($_POST['ma_sv']))
                           echo $_POST['ma_sv'];
                       ?>">
         </p>
         <p>
             Tên Sinh Viên :
             <input type="text" name="ho_ten" value="<?php  if(isset($_POST['ho_ten']))
                           echo $_POST['ho_ten'];
                       ?>">
         </p>
         <p>
             <input type="submit" name="sub" value="OK">
         </p>
      </form>

        <table border="1px">
             
             <?php  
               if(isset($_POST['sub'])) {?>
                  <tr>
                 <td>
                     Mã Sinh Viên
                 </td>
                 <td>
                     Tên Sinh Viên
                 </td>
                 <td>
                     Giới Tính
                 </td>
                 <td>
                     Khoa
                 </td>
             </tr>
               <?php
                 $sql = "SELECT sinh_vien.ma_sv, sinh_vien.ho_ten, sinh_vien.gioi_tinh, khoa.ten_khoa
                 FROM sinh_vien
                 INNER JOIN khoa
                 ON khoa.ma_khoa = sinh_vien.ma_khoa
                 WHERE ". $dk ;
                 $query = $db->query($sql);
                 $result = $query->fetch_all(MYSQLI_ASSOC);
                 foreach ($result as $sv) { ?>
                  <tr>
                      <td> <?php  echo $sv['ma_sv']; ?></td>
                      <td> <?php  echo $sv['ho_ten']; ?></td>
                      <td> <?php 
                             if ($sv['gioi_tinh'] == '1') {
                                echo "Nam";
                             }
                             else {
                                echo "Nữ";
                             }
                       ?></td>
                      <td> <?php  echo $sv['ten_khoa']; ?></td>
                  </tr> 
             <?php    }
           }
             ?>

             <?php  
                if (isset($_POST['sub'])) {
                     if (count($err) > 0) {
                        for ($i=0; $i < count($err) ; $i++) { 
                          echo $err[$i];
                        }
                     }
                
               else {
                         echo "Thành công";
               } 
               }
             ?>
        </table>   
  </div>

    </div>
   </div>
        
       </div>


      </div>
     

    <script src="js/jquery-3.2.1.min.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script> 
</body>
</html>