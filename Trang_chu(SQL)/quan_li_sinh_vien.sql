-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 26, 2018 at 05:20 PM
-- Server version: 10.1.29-MariaDB
-- PHP Version: 7.1.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `quan_li_sinh_vien`
--

-- --------------------------------------------------------

--
-- Table structure for table `ket_qua`
--

CREATE TABLE `ket_qua` (
  `diem` int(11) NOT NULL,
  `ma_sv` varchar(50) NOT NULL,
  `ma_mh` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ket_qua`
--

INSERT INTO `ket_qua` (`diem`, `ma_sv`, `ma_mh`) VALUES
(10, 'sv0702', 'mh01'),
(9, 'sv0702', 'mh02'),
(10, 'sv0702', 'mh03'),
(10, 'sv0702', 'mh04'),
(9, 'sv0702', 'mh05'),
(10, 'sv0702', 'mh06'),
(10, 'sv0702', 'mh07'),
(8, 'sv0702', 'mh08'),
(10, 'sv0702', 'mh09'),
(9, 'sv0702', 'mh10'),
(10, 'sv1001', 'mh01'),
(9, 'sv1003', 'mh02'),
(8, 'sv1003', 'mh03'),
(7, 'sv1004', 'mh04'),
(6, 'sv1005', 'mh05'),
(5, 'sv1006', 'mh06'),
(4, 'sv1007', 'mh07'),
(3, 'sv1008', 'mh08'),
(2, 'sv1009', 'mh09'),
(1, 'sv1010', 'mh10'),
(10, 'sv1011', 'mh01'),
(9, 'sv1023', 'mh02'),
(8, 'sv1024', 'mh03'),
(10, 'sv1025', 'mh05'),
(7, 'sv1027', 'mh06'),
(6, 'sv1032', 'mh01'),
(5, 'sv1033', 'mh01'),
(4, 'sv1035', 'mh01'),
(3, 'sv1037', 'mh01'),
(2, 'sv1038', 'mh01'),
(1, 'sv1039', 'mh02'),
(10, 'sv1037', 'mh02'),
(9, 'sv1041', 'mh02'),
(8, 'sv1042', 'mh02'),
(7, 'sv1043', 'mh02'),
(6, 'sv1044', 'mh06'),
(5, 'sv1045', 'mh06'),
(4, 'sv1046', 'mh06'),
(3, 'sv1047', 'mh06'),
(2, 'sv1048', 'mh07'),
(1, 'sv1049', 'mh07'),
(10, 'sv1050', 'mh07'),
(9, 'sv1012', 'mh07'),
(8, 'sv1013', 'mh08'),
(7, 'sv1014', 'mh08'),
(6, 'sv1015', 'mh08'),
(5, 'sv1016', 'mh08'),
(4, 'sv1017', 'mh09'),
(3, 'sv1018', 'mh09'),
(2, 'sv1019', 'mh09'),
(1, 'sv1020', 'mh10'),
(10, 'sv1021', 'mh10'),
(9, 'sv1022', 'mh10'),
(8, 'sv1023', 'mh04'),
(7, 'sv1024', 'mh04');

-- --------------------------------------------------------

--
-- Table structure for table `khoa`
--

CREATE TABLE `khoa` (
  `ma_khoa` varchar(50) NOT NULL,
  `ten_khoa` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `khoa`
--

INSERT INTO `khoa` (`ma_khoa`, `ten_khoa`) VALUES
('k01', 'Công Nghệ Thông Tin'),
('k02', 'Ngoại Ngữ'),
('k03', 'Kinh Tế'),
('k04', 'Cơ Khí'),
('k05', 'Cơ Điện Tử'),
('k06', 'Kinh Tế');

-- --------------------------------------------------------

--
-- Table structure for table `mon_hoc`
--

CREATE TABLE `mon_hoc` (
  `ma_mh` varchar(50) NOT NULL,
  `ten_mh` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mon_hoc`
--

INSERT INTO `mon_hoc` (`ma_mh`, `ten_mh`) VALUES
('mh01', 'Lập trình Căn Bản'),
('mh02', 'Tối Ưu Hóa'),
('mh03', 'Lập Trình Web'),
('mh04', 'Lập Trình Hướng Đối Tượng'),
('mh05', 'Tiếng Anh'),
('mh06', 'Toán Cao Cấp'),
('mh07', 'Vật Lí'),
('mh08', 'Triết Học'),
('mh09', 'Kinh Tế Đại Cương'),
('mh10', 'Kĩ Năng Giao Tiếp');

-- --------------------------------------------------------

--
-- Table structure for table `sinh_vien`
--

CREATE TABLE `sinh_vien` (
  `ma_sv` varchar(50) NOT NULL,
  `ho_ten` varchar(200) NOT NULL,
  `ngay_sinh` varchar(200) NOT NULL,
  `gioi_tinh` varchar(11) NOT NULL,
  `email` varchar(200) NOT NULL,
  `dia_chi` varchar(200) NOT NULL,
  `ma_khoa` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sinh_vien`
--

INSERT INTO `sinh_vien` (`ma_sv`, `ho_ten`, `ngay_sinh`, `gioi_tinh`, `email`, `dia_chi`, `ma_khoa`) VALUES
('sv0702', 'Vũ Ngọc Truy', '1998-02-07', '1', 'twolffox@gmail.com', 'Văn Giang - Hưng Yên', 'k01'),
('sv1001', 'Trần Tuấn Anh', '1998-05-30', '1', 'tun@gmail.com', 'Vĩnh Phúc', 'k02'),
('sv1003', 'Nguyễn Thị Tươi', '1998-03-15', '0', 'tuoi@gmail.com', 'Bắc Giang', 'k03'),
('sv1004', 'Trần Tuấn Văn', '1998-05-01', '1', 'tun01@gmail.com', 'Vĩnh Phúc', 'k02'),
('sv1005', 'Trần Ngọc Anh', '1998-05-02', '1', 'tun02@gmail.com', 'Quảng Ninh', 'k02'),
('sv1006', 'Vũ Cao Tuấn', '1998-05-03', '1', 'tun03@gmail.com', 'Thanh Hóa', 'k02'),
('sv1007', 'Vũ Văn Cường', '1998-05-04', '1', 'tun04@gmail.com', 'Nghệ An', 'k02'),
('sv1008', 'Vũ Văn Long', '1998-05-05', '1', 'tun05@gmail.com', 'Hà Nội', 'k02'),
('sv1009', 'Nguyễn Văn Mạnh', '1998-05-06', '1', 'tun06@gmail.com', 'Hà Tĩnh', 'k02'),
('sv1010', 'Nguyễn Văn Dũng', '1998-02-07', '1', 'tun07@gmail.com', 'Huế', 'k02'),
('sv1011', 'Nguyễn Ngọc Thiều', '1998-02-08', '1', 'tun08@gmail.com', 'Bình Dương', 'k02'),
('sv1012', 'Nguyễn Ngọc Quang', '1998-02-09', '1', 'tun09@gmail.com', 'Bình Phước', 'k02'),
('sv1013', 'Cao Ngọc Trung', '1998-02-10', '1', 'tun10@gmail.com', 'Nam Định', 'k03'),
('sv1014', 'Cao Ngọc Hà', '1998-02-11', '1', 'tun11@gmail.com', 'Quảng Nam', 'k03'),
('sv1015', 'Cao Xuân Hải', '1998-02-12', '1', 'tun12@gmail.com', 'Điện Biên', 'k03'),
('sv1016', 'Cao Xuân Trọng', '1998-03-13', '1', 'tun13@gmail.com', 'Hải Dương', 'k03'),
('sv1017', 'Ngô Xuân Tuân', '1998-03-14', '1', 'tun14@gmail.com', 'Hải Phòng', 'k03'),
('sv1018', 'Ngô Xuân Đức', '1998-03-15', '1', 'tun15@gmail.com', 'Qủang Ngãi', 'k03'),
('sv1019', 'Ngô Quang Hải', '1998-03-16', '1', 'tun16@gmail.com', 'Quảng Trị', 'k03'),
('sv1020', 'Ngô Quang Đức', '1998-03-17', '1', 'tun17@gmail.com', 'Vĩnh Phúc', 'k03'),
('sv1021', 'Ngô Quang Tài', '1998-03-18', '1', 'tun18@gmail.com', 'Long An', 'k03'),
('sv1022', 'Phạm Quang Lê', '1998-03-19', '1', 'tun19@gmail.com', 'Long Xuyên', 'k03'),
('sv1023', 'Phạm Trung Toán', '1998-03-20', '1', 'tun20@gmail.com', 'Vĩnh Long', 'k04'),
('sv1024', 'Phạm Trung Hiếu', '1998-04-21', '1', 'tun21@gmail.com', 'Hưng Yên', 'k04'),
('sv1025', 'Phạm Trung Phương', '1998-04-22', '1', 'tun22@gmail.com', 'Hưng Yên', 'k04'),
('sv1026', 'Phạm Trung Dương', '1998-04-23', '1', 'tun23@gmail.com', 'Vĩnh Phúc', 'k04'),
('sv1027', 'Lê Tiến Luật', '1998-04-24', '1', 'tun24@gmail.com', 'Vĩnh Phúc', 'k04'),
('sv1028', 'Lê Quốc Lương', '1998-04-25', '1', 'tun25@gmail.com', 'Vĩnh Phúc', 'k04'),
('sv1029', 'Trần Thị Kiều Anh', '1998-04-26', '0', 'tun26@gmail.com', 'Vĩnh Phúc', 'k04'),
('sv1030', 'Trần Thị Hồng', '1998-04-27', '0', 'tun27@gmail.com', 'Vĩnh Phúc', 'k04'),
('sv1031', 'Trần Thị Đào', '1998-06-28', '0', 'tun28@gmail.com', 'Vĩnh Phúc', 'k04'),
('sv1032', 'Trần Thị Cúc', '1998-06-29', '0', 'tun29@gmail.com', 'Hưng Yên', 'k04'),
('sv1033', 'Trần Thị Trúc', '1998-06-30', '0', 'tun30@gmail.com', 'Hưng Yên', 'k01'),
('sv1034', 'Trần Thị Mai', '1998-06-01', '0', 'tun31@gmail.com', 'Hưng Yên', 'k01'),
('sv1035', 'Phạm Mỹ Hạnh', '1998-06-02', '0', 'tun32@gmail.com', 'Hưng Yên', 'k01'),
('sv1036', 'Phạm Mỹ Hợp', '1998-06-03', '0', 'tun33@gmail.com', 'Hưng Yên', 'k01'),
('sv1037', 'Phạm Mỹ Dung', '1998-07-04', '0', 'tun34@gmail.com', 'Hải Phòng', 'k01'),
('sv1038', 'Phạm Mỹ Vân', '1998-07-05', '0', 'tun35@gmail.com', 'Hải Phòng', 'k01'),
('sv1039', 'Nguyễn Mỹ Kiều', '1998-07-06', '0', 'tun36@gmail.com', 'Hải Phòng', 'k01'),
('sv1040', 'Nguyễn Ngọc Thúy', '1998-07-07', '0', 'tun37@gmail.com', 'Hải Phòng', 'k01'),
('sv1041', 'Nguyễn Ngọc Thùy', '1998-11-08', '0', 'tun38@gmail.com', 'Hải Phòng', 'k01'),
('sv1042', 'Nguyễn Ngọc Thanh', '1998-11-09', '0', 'tun39@gmail.com', 'Nam Định', 'k01'),
('sv1043', 'Nguyễn Ngọc Như', '1998-11-10', '0', 'tun40@gmail.com', 'Nam Định', 'k05'),
('sv1044', 'Vũ Ngọc Huyền', '1998-11-11', '0', 'tun41@gmail.com', 'Nam Định', 'k05'),
('sv1045', 'Vũ Phương Yến', '1998-11-12', '0', 'tun42@gmail.com', 'Nam Định', 'k05'),
('sv1046', 'Vũ Phương Trang', '1998-12-13', '0', 'tun42@gmail.com', 'Nam Định', 'k05'),
('sv1047', 'Vũ Phương Trinh', '1998-12-14', '0', 'tun44@gmail.com', 'Hà Nội', 'k05'),
('sv1048', 'Cao Phương My', '1998-12-15', '0', 'tun45@gmail.com', 'Hà Nội', 'k05'),
('sv1049', 'Cao Hoài Mến', '1998-12-16', '0', 'tun46@gmail.com', 'Hà Nội', 'k05'),
('sv1050', 'Cao Hoài Mận', '1998-12-17', '0', 'tun47@gmail.com', 'Hà Nội', 'k05'),
('sv1051', 'Cao Hoài Thương', '1998-09-18', '0', 'tun48@gmail.com', 'Quảng Ninh', 'k05'),
('sv1052', 'Cao Hoài Hương', '1998-09-19', '0', 'tun49@gmail.com', 'Quảng Ninh', 'k05'),
('sv1053', 'Đào Thị Thoan', '1998-09-20', '0', 'tun50@gmail.com', 'Quảng Ninh', 'k06'),
('sv1054', 'Đào Thị Mơ', '1998-09-21', '0', 'tun51@gmail.com', 'Quảng Ninh', 'k06'),
('sv1055', 'Đào Thị Dung', '1998-09-22', '0', 'tun52@gmail.com', 'Bắc Giang', 'k06'),
('sv1056', 'Đào Mai Ngọc', '1998-10-23', '0', 'tun53@gmail.com', 'Bắc Giang', 'k06'),
('sv1057', 'Trần Mai An', '1998-10-24', '0', 'tun54@gmail.com', 'Bắc Giang', 'k06'),
('sv1058', 'Trần Mai Ánh', '1998-10-25', '0', 'tun55@gmail.com', 'Bắc Giang', 'k06'),
('sv1059', 'Trần Mai Tuyết', '1998-10-30', '0', 'tun56@gmail.com', 'Bắc Giang', 'k06');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ket_qua`
--
ALTER TABLE `ket_qua`
  ADD KEY `ma_sv` (`ma_sv`),
  ADD KEY `ma_mh` (`ma_mh`);

--
-- Indexes for table `khoa`
--
ALTER TABLE `khoa`
  ADD PRIMARY KEY (`ma_khoa`);

--
-- Indexes for table `mon_hoc`
--
ALTER TABLE `mon_hoc`
  ADD PRIMARY KEY (`ma_mh`);

--
-- Indexes for table `sinh_vien`
--
ALTER TABLE `sinh_vien`
  ADD PRIMARY KEY (`ma_sv`),
  ADD KEY `ma_khoa` (`ma_khoa`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `ket_qua`
--
ALTER TABLE `ket_qua`
  ADD CONSTRAINT `ket_qua_ibfk_1` FOREIGN KEY (`ma_mh`) REFERENCES `mon_hoc` (`ma_mh`),
  ADD CONSTRAINT `ket_qua_ibfk_2` FOREIGN KEY (`ma_sv`) REFERENCES `sinh_vien` (`ma_sv`);

--
-- Constraints for table `sinh_vien`
--
ALTER TABLE `sinh_vien`
  ADD CONSTRAINT `sinh_vien_ibfk_1` FOREIGN KEY (`ma_khoa`) REFERENCES `khoa` (`ma_khoa`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
