<?php  
       $err = [];
       session_start(); 
      if (!isset($_SESSION['user'])) {
         header("Location: sinh_vien_login.php");
       } 
       require('connect.php');
       // Lấy dữ liệu
       $masv = $_GET['ma_sv'];
       $sql = "SELECT * FROM sinh_vien WHERE ma_sv = '{$masv}' LIMIT 1";
       $query = $db->query($sql);
       $sv = $query->fetch_assoc();
       if (is_null($sv)) {
       	 header('Location: sinh_vien.php');
       }	 
       if (isset($_POST['deny'])) {
       	   header('Location: sinh_vien.php');
       }
      
       if (isset($_POST['sub'])) {  
            $sql = "DELETE FROM ket_qua WHERE ma_sv = '". $sv['ma_sv'] ."'" ;
            $query = $db->query($sql);
            if (!$query) {
                $err[] = "Không thể xóa thao tác bị lỗi";
            }
            $sql = "DELETE FROM sinh_vien WHERE ma_sv = '". $sv['ma_sv'] ."'";
            $query = $db->query($sql);
            if (!$query) {
                $err[] = "Không thể xóa thao tác bị lỗi";
            }
       }     
?>
<!DOCTYPE html>
<html>
<head>
	<title>Xóa Sinh Viên</title>

	<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="css/fontawesome-all.min.css">
    <link rel="stylesheet" type="text/css" href="css/owl.carousel.css">
   <link rel="yylesheet" type="text/css" href="css/owl.theme.default.min.css">
  <link rel="stylesheet" type="text/css" href="css/style_trangchu.css">
</head>
<body>
  
      <div class="container">
      	 <div class="row head">
      	 	 <div class="col-md-2 logo">
      	 	 	<img src="img/logo.png">
      	 	 </div>
      	 	 <div class="col-md-10 tieude">
      	 	 	<center><h3>Xóa Sinh Viên</h3></center>
      	 	 </div>
      	 </div>
 
      	<div class="menu">
      		<ul class="row">
      			<li class="col-md-3">
      				<center><a href="trang_chu.php">Trang chủ</a></center>
      			</li>
              
      			<li class="col-md-3">
      				<center><a href="sinh_vien.php?page_now=0">Sinh viên</a></center>
      			</li>

      			<li class="col-md-3">
      				<center><a href="khoa.php">Khoa</a></center>
      			</li>

      			<li class="col-md-3">
      				<center><a href="mon.php">Môn học</a></center>
      			</li>
      		</ul>


      	</div> 
      	<form action="" method="POST">
          
         <?php  
            if (isset($_POST['sub'])) {

          
              if (count($err) > 0) {
                 for ($i=0; $i < count($err) ; $i++) { 
                      echo $err[$i];
                 }
              }

              else echo "Thành Công";

               } 
         ?>
             <h3>Xóa</h3>
             <table border="1px">
             	   <tr>
             	   	  <td>
             	   	  	Mã Sinh Viên
             	   	  </td>
             	   	  <td>
             	   	  	 Tên Sinh Viên 
             	   	  </td>
                    <td>
                       Ngày Sinh 
                    </td>
                    <td width="100px">
                       Giới Tính 
                    </td>
                    <td>
                       Email
                    </td>
                    <td>
                       Địa Chỉ
                    </td>
                    <td>
                       Khoa
                    </td>
             	   </tr>
             	   <tr>
             	   	  <td>
             	   	    <input readonly="readonly" type="text" name="ma_sv" value=" <?php 
                                  if(isset($_POST['ma_sv'])) {
                                  	  echo($_POST['ma_sv']);
                                  }
                                  else
                                  	 echo($sv['ma_sv']);
             	   	     ?> ">
             	   	  </td>
                    <td>
                       <input readonly="readonly" type="text" name="ho_ten" value =" <?php  
                                if (isset($_POST['ho_ten'])) {
                                      echo   $_POST['ho_ten'];
                                }
                                else echo $sv['ho_ten'];
                      ?>">
                    </td>
                    <td>
                       <input readonly="readonly" type="text" name="ngay_sinh" value =" <?php  
                                if (isset($_POST['ngay_sinh'])) {
                                      echo   $_POST['ngay_sinh'];
                                }
                                else echo $sv['ngay_sinh'];
                      ?>">
                    </td>
                    <td width="100px">
                         <input readonly="readonly" type="text" name="gioi_tinh" value=" <?php 
                               if($sv['gioi_tinh'] == '1') {
                               	  echo "Nam";
                               }
                               else {
                               	  echo "Nữ";
                               }
                          ?> ">
                    </td>
                    <td>
                        <input readonly="readonly" type="text" name="email" value =" <?php  
                                if (isset($_POST['email'])) {
                                      echo   $_POST['email'];
                                }
                                else echo $sv['email'];
                      ?>">
                    </td>
                    <td>
                        <input readonly="readonly" type="text" name="dia_chi" value =" <?php  
                                if (isset($_POST['dia_chi'])) {
                                      echo   $_POST['dia_chi'];
                                }
                                else echo $sv['dia_chi'];
                      ?>">
                    </td>
                    <td>
                        <input readonly="readonly" type="text" name="ma_khoa" value="
                           <?php  
                                echo $sv['ma_khoa'];
                            ?>
                        ">
                    </td>
             	   </tr>
             </table>
            <br>
            <br>
            <h3>Bạn có đồng ý xóa sinh viên trên không?</h3>
            <br>
             <p>
                <input type="submit" name="sub" value="XÓA">
          
             	<input type="submit" name="deny" value="KHÔNG XÓA NỮA">
             </p>
         </form>


       </div>

     

    <script src="js/jquery-3.2.1.min.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script>	
</body>
</html>