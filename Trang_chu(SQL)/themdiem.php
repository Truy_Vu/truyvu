<?php  
      $err = []; 
      session_start(); 
      if (!isset($_SESSION['user'])) {
         header("Location: sinh_vien_login.php");
       } 
       require('connect.php');
       if (isset($_POST['sub'])) {
            if (!isset($_POST['ma_mh']) || $_POST['ma_mh'] == '') {
                 $err[] ='Hãy chọn mã môn học<br>';
            }
            if (!isset($_POST['ma_sv']) || $_POST['ma_sv'] == '') {
                 $err[] ='Hãy chọn mã sinh viên<br>';
            }
            if (!isset($_POST['diem']) || $_POST['diem'] == '') {
                 $err[] ='Điểm bị bỏ trống<br>';
            }
         if (count($err) == 0) {
         	    $ma_sv = $_POST['ma_sv'];
         	    $ma_mh = $_POST['ma_mh'];
                $diem = $_POST['diem'];
                $sql = "INSERT INTO ket_qua (diem, ma_sv, ma_mh) VALUES ('" . $diem . "','" . $ma_sv . "','" . $ma_mh . "')";
                $query = $db->query($sql);
         }
       }
      
?>
<!DOCTYPE html>
<html>
<head>
	<title>Thêm Điểm</title>

	<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="css/fontawesome-all.min.css">
    <link rel="stylesheet" type="text/css" href="css/owl.carousel.css">
   <link rel="yylesheet" type="text/css" href="css/owl.theme.default.min.css">
  <link rel="stylesheet" type="text/css" href="css/style_trangchu.css">
</head>
<body>
      <div class="container">
      	 <div class="row head">
      	 	 <div class="col-md-2 logo">
      	 	 	<img src="img/logo.png">
      	 	 </div>
      	 	 <div class="col-md-10 tieude">
      	 	 	<center><h3>Thêm điểm</h3></center>
      	 	 </div>
      	 </div>

      	<div class="menu">
      		<ul class="row">
      			<li class="col-md-3">
      				<center><a href="trang_chu.php">Trang chủ</a></center>
      			</li>
              
      			<li class="col-md-3">
      				<center><a href="sinh_vien.php?page_now=0">Sinh viên</a></center>
      			</li>

      			<li class="col-md-3">
      				<center><a href="khoa.php">Khoa</a></center>
      			</li>

      			<li class="col-md-3">
      				<center><a href="mon.php">Môn học</a></center>
      			</li>
      		</ul>
      	</div>       	
       
        <h3>Thêm Điểm</h3>
        <form action="" method="POST">
        	 <p>
         	Mã Môn :
         	 <select name="ma_mh">
         	 	     <option value="">-Chọn-</option>
         	 	 <?php  
                        $sql = "SELECT * FROM mon_hoc";
                        $query = $db->query($sql);
                        $result = $query->fetch_all(MYSQLI_ASSOC);
                        if (count($result) > 0) {
                        	foreach ($result as $mon_hoc) {
                        	     echo "<option value = '".$mon_hoc['ma_mh']."'>" . $mon_hoc['ma_mh'] . "</option>";
                        	}
                        }
         	 	 ?>
         	 </select>
         </p>
         <p>
         	Mã Sinh Viên :
         	   <select name="ma_sv">
         	 	     <option value="">-Chọn-</option>
         	 	 <?php  
                        $sql = "SELECT * FROM sinh_vien";
                        $query = $db->query($sql);
                        $result = $query->fetch_all(MYSQLI_ASSOC);
                        if (count($result) > 0) {
                        	foreach ($result as $sinh_vien) {
                        	     echo "<option value = '".$sinh_vien['ma_sv']."'>" . $sinh_vien['ma_sv'] . "</option>";
                        	}
                        }
         	 	 ?>
         	 </select>
         </p>
         <p>
         	Điểm :
         	<input type="text" name="diem">
         </p>
         <p>
         	<input type="submit" name="sub" value="OKE">
         </p>
        </form>
      </div>
         <?php  
            if (isset($_POST['sub'])) {
            	 if (count($err) > 0) {
		                 for ($i=0; $i < count($err); $i++) { 
		                 	  echo $err[$i];
		                 }
	             }
	             else {
	                if ($query) {
	                		echo "Thành công";
	                } 
	                else {
	                	    echo "Không thể thêm, thao tác bị lỗi";
	                }
	             }

            }
         ?>

    <script src="js/jquery-3.2.1.min.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script>	
</body>
</html>