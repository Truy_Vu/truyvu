<?php  
       $err = [];
       require('connect.php');
       session_start(); 
       if (!isset($_SESSION['user'])) {
         header("Location: sinh_vien_login.php");
       } 
       $ma_khoa = $_GET['ma_khoa'];
       $sql = "SELECT * FROM khoa WHERE ma_khoa = '{$ma_khoa}' LIMIT 1";
       $query = $db->query($sql);
       $khoa = $query->fetch_assoc();
       if (is_null($khoa)) {
       	 header('Location: khoa.php');
       }	 
       if (isset($_POST['deny'])) {
       	   header('Location: khoa.php');
       }
      
       if (isset($_POST['sub'])) {  
           $sql = "DELETE FROM ket_qua WHERE ma_sv IN 
              (
                  SELECT ma_sv FROM sinh_vien
                  WHERE ma_khoa = '". $khoa['ma_khoa'] ."' 
              )
            ";
           $query = $db->query($sql); 
           if (!$query) {
              $err[] = 'Lỗi, không thể xóa';
           }

           $sql = "DELETE FROM sinh_vien WHERE ma_khoa = '". $khoa['ma_khoa'] ."'
           ";
           $query = $db->query($sql);
           if (!$query) {
              $err[] = 'Lỗi, không thể xóa';
           }
           
            $sql = "DELETE FROM khoa WHERE ma_khoa = '". $khoa['ma_khoa'] ."'
            ";

           $query = $db->query($sql);
           if (!$query) {
              $err[] = 'Lỗi, không thể xóa';
           } 
       }     
?>
<!DOCTYPE html>
<html>
<head>
	<title>Xóa Sinh Viên</title>

	<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="css/fontawesome-all.min.css">
    <link rel="stylesheet" type="text/css" href="css/owl.carousel.css">
   <link rel="yylesheet" type="text/css" href="css/owl.theme.default.min.css">
  <link rel="stylesheet" type="text/css" href="css/style_trangchu.css">
</head>
<body>
  
      <div class="container">
      	 <div class="row head">
      	 	 <div class="col-md-2 logo">
      	 	 	<img src="img/logo.png">
      	 	 </div>
      	 	 <div class="col-md-10 tieude">
      	 	 	<center><h3>Xóa Sinh Viên</h3></center>
      	 	 </div>
      	 </div>
 
      	<div class="menu">
      		<ul class="row">
      			<li class="col-md-3">
      				<center><a href="trang_chu.php">Trang chủ</a></center>
      			</li>
              
      			<li class="col-md-3">
      				<center><a href="ssinh_vien.php?page_now=0">Sinh viên</a></center>
      			</li>

      			<li class="col-md-3">
      				<center><a href="khoa.php">Khoa</a></center>
      			</li>

      			<li class="col-md-3">
      				<center><a href="mon.php">Môn học</a></center>
      			</li>
      		</ul>


      	</div> 
      	<form action="" method="POST">
          
         <?php  
            if (isset($_POST['sub'])) {

          
              if (count($err) > 0) {
                 for ($i=0; $i < count($err) ; $i++) { 
                      echo $err[$i];
                 }
              }

              else echo "Thành Công";

               } 
         ?>
             <h3>Xóa</h3>
             <table border="1px">
             	   <tr>
             	   	  <td>
             	   	  	Mã Khoa
             	   	  </td>
             	   	  <td>
             	   	  	 Tên Khoa 
             	   	  </td>                  
             	   </tr>
             	   <tr>
             	   	  <td>
             	   	    <input readonly="readonly" type="text" name="ma_khoa" value=" <?php 
                                  if(isset($_POST['ma_khoa'])) {
                                  	  echo($_POST['ma_khoa']);
                                  }
                                  else
                                  	 echo($khoa['ma_khoa']);
             	   	     ?> ">
             	   	  </td>
                    <td>
                       <input readonly="readonly" type="text" name="ten_khoa" value =" <?php  
                                if (isset($_POST['ten_khoa'])) {
                                      echo   $_POST['ten_khoa'];
                                }
                                else echo $khoa['ten_khoa'];
                      ?>">
                    </td>                  
             	   </tr>
             </table>
            <br>
            <br>
            <h3>Bạn có đồng ý xóa sinh viên trên không?</h3>
            <br>
             <p>
                <input type="submit" name="sub" value="XÓA">
          
             	<input type="submit" name="deny" value="KHÔNG XÓA NỮA">
             </p>
         </form>


       </div>

     

    <script src="js/jquery-3.2.1.min.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script>	
</body>
</html>