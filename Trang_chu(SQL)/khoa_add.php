<?php  
      $err = [];
      
       require('connect.php');
       session_start(); 
      if (!isset($_SESSION['user'])) {
         header("Location: sinh_vien_login.php");
       } 
       if (isset($_POST['sub'])) {
          if (!isset($_POST['ma_khoa']) || !isset($_POST['ten_khoa']) ||$_POST['ma_khoa'] == '' || $_POST['ten_khoa'] == '') {
           $err[] = 'Mã khoa hoặc tên khoa bị bỏ trống';
          }
        $ma = trim($_POST['ma_khoa']);
        $sql = "SELECT ma_khoa FROM khoa WHERE ma_khoa = '".$ma."'";
        $query = $db->query($sql);
        $result = $query->fetch_all(MYSQLI_ASSOC);
        if (count($result) >0) {
         $err[] = 'Mã khoa đã tồn tại';
        }
         if (count($err) == 0) {
         	 $makhoa = $_POST['ma_khoa'];
             $ten_khoa = $_POST['ten_khoa'];
             $sql = "INSERT INTO khoa (ma_khoa, ten_khoa) VALUES('".$makhoa."','".$ten_khoa."')";
             $query = $db->query($sql);
            
         }
       }
      
?>
<!DOCTYPE html>
<html>
<head>
	<title>Thêm Khoa</title>

	<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="css/fontawesome-all.min.css">
    <link rel="stylesheet" type="text/css" href="css/owl.carousel.css">
   <link rel="yylesheet" type="text/css" href="css/owl.theme.default.min.css">
  <link rel="stylesheet" type="text/css" href="css/style_trangchu.css">
</head>
<body>
      <div class="container">
      	 <div class="row head">
      	 	 <div class="col-md-2 logo">
      	 	 	<img src="img/logo.png">
      	 	 </div>
      	 	 <div class="col-md-10 tieude">
      	 	 	<center><h3>Thêm khoa</h3></center>
      	 	 </div>
      	 </div>

      	<div class="menu">
      		<ul class="row">
      			<li class="col-md-3">
      				<center><a href="trang_chu.php">Trang chủ</a></center>
      			</li>
              
      			<li class="col-md-3">
      				<center><a href="sinh_vien.php?page_now=0">Sinh viên</a></center>
      			</li>

      			<li class="col-md-3">
      				<center><a href="khoa.php">Khoa</a></center>
      			</li>

      			<li class="col-md-3">
      				<center><a href="mon.php">Môn học</a></center>
      			</li>
      		</ul>
      	</div>       	
       
        <h3>Thêm Khoa</h3>
        <form action="" method="POST">
        	 <p>
         	Mã khoa :
         	<input type="text" name="ma_khoa">
         </p>
         <p>
         	Tên Khoa :
         	<input type="text" name="ten_khoa">
         </p>
         <p>
         	<input type="submit" name="sub" value="OKE">
         </p>
        </form>
      </div>
         <?php  
            if (isset($_POST['sub'])) {
            	 if (count($err) > 0) {
		                 for ($i=0; $i < count($err); $i++) { 
		                 	  echo $err[$i];
		                 }
	             }
	             else {
	                if ($query) {
	                		echo "Thành công";
	                } 
	                else {
	                	    echo "Không thể thêm, thao tác bị lỗi";
	                }
	             }

            }
         ?>

    <script src="js/jquery-3.2.1.min.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script>	
</body>
</html>