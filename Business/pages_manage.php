<?php  
   require('connect.php');
   $err = []; 
   
?>
<!DOCTYPE html>
<html>
<head>
	<title>Manage pages</title>

	<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="css/fontawesome-all.min.css">
    <link rel="stylesheet" type="text/css" href="css/owl.carousel.css">
   <link rel="yylesheet" type="text/css" href="css/owl.theme.default.min.css">
  <link rel="stylesheet" type="text/css" href="css/style_trangchu.css">
</head>
<body>
     <center>
     	<h1>Quản Lí Sản Phẩm</h1>
     </center>
	<br>
	<br>
	<br>

	<br>
	<div class="container">
      <a href="pages_add.php">Thêm sản phẩm</a>
      <br>
      <br>
      <table border="1px" class="text-center">
      	  <tr>
      	  	 <td>
              Name
            </td>
            <td>
              Slug
            </td>
            <td>
              Img
            </td>
            <td>
              Description
            </td>
            <td>
             Content
            </td>
      	  	<td>Edit</td>
      	  </tr>
           <?php 
              
                $sql = "SELECT * FROM pages";         
                $query = $db->query($sql);
                $result = $query->fetch_all(MYSQLI_ASSOC);
                foreach ($result as $page) { ?>
            <tr>
            	<td><?php echo $page['name']; ?></td>
            	<td><?php echo $page['slug']; ?></td>
            	<td><?php echo $page['img']; ?></td>
            	<td><?php echo $page['description']; ?></td>
            	<td><?php echo $page['content']; ?></td>
                <td>
                	  <p><a href="pages_edit.php?id=<?php echo $page['id'] ?>">Cập nhật thông tin</a></p>
                     <p><a href="pages_delete.php?id=<?php echo $page['id'] ?>">Hủy Kinh Doanh</a></p>
                      <p>

                           <a href="pages_status.php?id=<?php echo $page['id'] ?>">Điều chỉnh trạng thái</a>     
                      </p>
                </td>
            </tr>    
            <?php    
                }
            ?>
      </table>
     </div>
    <script src="js/jquery-3.2.1.min.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script>	
</body>
</html>