<?php  
   require('connect.php');
   $err = []; 
   
?>
<!DOCTYPE html>
<html>
<head>
	<title>Manage Admin</title>

	<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="css/fontawesome-all.min.css">
    <link rel="stylesheet" type="text/css" href="css/owl.carousel.css">
   <link rel="yylesheet" type="text/css" href="css/owl.theme.default.min.css">
  <link rel="stylesheet" type="text/css" href="css/style_trangchu.css">
</head>
<body>
     <center>
     	<h1>Quản Lí Admin</h1>
     </center>
	<br>
	<br>
	<br>

	<br>
	<div class="container">
      <a href="admin_add.php">Thêm Admin</a>
      <br>
      <br>
      <table border="1px" class="text-center">
      	  <tr>
      	  	<td>
      	  		Id 
      	  	</td>
      	  	<td>
      	  		User Name
      	  	</td>
      	  	<td>
      	  		Full Name
      	  	</td>
      	  	<td>
      	  		Email
      	  	</td>
      	  	<td>
      	  		Roles
      	  	</td>
      	  	<td>
      	  		Trạng thái
      	  	</td>
      	  	<td>
      	  		Edit
      	  	</td>
      	  </tr>
           <?php 
              
                $sql = "SELECT * FROM admin";         
                $query = $db->query($sql);
                $result = $query->fetch_all(MYSQLI_ASSOC);
                foreach ($result as $admin) { ?>
            <tr>
            	<td><?php echo $admin['id']; ?></td>
            	<td><?php echo $admin['username']; ?></td>
            	<td><?php echo $admin['fullname'] ?></td>
            	<td><?php echo $admin['email']  ?></td>
            	<td><?php echo $admin['roles']; ?></td>
            	<td>
		                <p>
		                  <?php  if ($admin['status'] == 1) {
		                        		    echo "Đang hoạt động";
		                        	} else {
		                                echo "Không hoạt động";
		                          } ?>	
		                 </p>
            	</td>
                <td>
                	  <p><a href="admin_edit.php?id=<?php echo $admin['id'] ?>">Cập nhật thông tin</a></p>
                     <p><a href="admin_delete.php?id=<?php echo $admin['id'] ?>">Xóa</a></p>
                </td>
            </tr>    
            <?php    
                }
            ?>
      </table>
     </div>
    <script src="js/jquery-3.2.1.min.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script>	
</body>
</html>