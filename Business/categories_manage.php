<?php  
   require('connect.php');
   $err = []; 
   
?>
<!DOCTYPE html>
<html>
<head>
	<title>Manage categories</title>

	<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="css/fontawesome-all.min.css">
    <link rel="stylesheet" type="text/css" href="css/owl.carousel.css">
   <link rel="yylesheet" type="text/css" href="css/owl.theme.default.min.css">
  <link rel="stylesheet" type="text/css" href="css/style_trangchu.css">
</head>
<body>
     <center>
     	<h1>Quản Lí Sản Phẩm</h1>
     </center>
	<br>
	<br>
	<br>

	<br>
	<div class="container">
      <a href="products_add.php">Thêm sản phẩm</a>
      <br>
      <br>
      <table border="1px" class="text-center">
      	  <tr>
      	  	 <td>
              Name
            </td>
            <td>
              Slug
            </td>
            <td>
              Parrent_id
            </td>
            <td>
              Description
            </td>
            <td>
             Image
            </td>
      	  	<td>Edit</td>
      	  </tr>
           <?php 
              
                $sql = "SELECT * FROM categories";         
                $query = $db->query($sql);
                $result = $query->fetch_all(MYSQLI_ASSOC);
                foreach ($result as $categorie) { ?>
            <tr>
            	<td><?php echo $categorie['name']; ?></td>
            	<td><?php echo $categorie['slug']; ?></td>
            	<td><?php echo $categorie['parrent_id']; ?></td>
            	<td><?php echo $categorie['description']; ?></td>
            	<td><?php echo $categorie['image']; ?></td>
                <td>
                	  <p><a href="categories_edit.php?id=<?php echo $categorie['id'] ?>">Cập nhật thông tin</a></p>
                     <p><a href="categories_delete.php?id=<?php echo $categorie['id'] ?>">Hủy Kinh Doanh</a></p>
                      <p>

                           <a href="categories_status.php?id=<?php echo $categorie['id'] ?>">Điều chỉnh trạng thái</a>     
                      </p>
                </td>
            </tr>    
            <?php    
                }
            ?>
      </table>
     </div>
    <script src="js/jquery-3.2.1.min.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script>	
</body>
</html>