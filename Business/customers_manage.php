<?php  
   require('connect.php');
   $err = []; 
   
?>
<!DOCTYPE html>
<html>
<head>
	<title>Manage Admin</title>

	<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="css/fontawesome-all.min.css">
    <link rel="stylesheet" type="text/css" href="css/owl.carousel.css">
   <link rel="yylesheet" type="text/css" href="css/owl.theme.default.min.css">
  <link rel="stylesheet" type="text/css" href="css/style_trangchu.css">
</head>
<body>
     <center>
     	<h1>Quản Lí Admin</h1>
     </center>
	<br>
	<br>
	<br>

	<br>
	<div class="container">
      <a href="customers_add.php">Thêm </a>
      <br>
      <br>
      <table border="1px" class="text-center">
      	  <tr>
      	  	<td>
      	  		Full Name
      	  	</td>
      	  	<td>
      	  		Tel
      	  	</td>
      	  	<td>
      	  	   Email
      	  	</td>
      	  	<td>
      	  		 Address
      	  	</td>
      	  	<td>
      	  		  Sex
      	  	</td>
      	  	<td>
      	  		Pass
      	  	</td>
      	  	<td>
      	  		Customer Group
      	  	</td>
            <td>
              Province
            </td>
            <td>
              District
            </td>
            <td>
              Day created
            </td>
            <td>
              Status
            </td>
            <td>
              Edit
            </td>
      	  </tr>
           <?php 
              
                $sql = "SELECT * FROM customers";         
                $query = $db->query($sql);
                $result = $query->fetch_all(MYSQLI_ASSOC);
                foreach ($result as $customer) { ?>
            <tr>
            	<td><?php echo $customer['name']; ?></td>
            	<td><?php echo $customer['tel']; ?></td>
            	<td><?php echo $customer['email'] ?></td>
              <td><?php echo $customer['address']; ?></td>
            	<td><?php echo $customer['sex']  ?></td>
              <td><?php echo $customer['pass']; ?></td>
            	<td><?php 
                      
               ?></td>
              <td><?php echo $customer['province_id']; ?></td>
              <td><?php echo $customer['district_id']; ?></td>
              <td><?php echo $customer['created_at']; ?></td>
            	<td>
		                <p>
		                  <?php  if ($customer['status'] == 1) {
		                        		    echo "Đang hoạt động";
		                        	} else {
		                                echo "Không hoạt động";
		                          } ?>	
		                 </p>
            	</td>
                <td>
                	  <p><a href="admin_edit.php?id=<?php echo $admin['id'] ?>">Cập nhật thông tin</a></p>
                     <p><a href="admin_delete.php?id=<?php echo $admin['id'] ?>">Xóa</a></p>
                </td>
            </tr>    
            <?php    
                }
            ?>
      </table>
     </div>
    <script src="js/jquery-3.2.1.min.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script>	
</body>
</html>