<?php  
   require('connect.php');
   $err = []; 
   
?>
<!DOCTYPE html>
<html>
<head>
	<title>Manage Products</title>

	<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="css/fontawesome-all.min.css">
    <link rel="stylesheet" type="text/css" href="css/owl.carousel.css">
   <link rel="yylesheet" type="text/css" href="css/owl.theme.default.min.css">
  <link rel="stylesheet" type="text/css" href="css/style_trangchu.css">
</head>
<body>
     <center>
     	<h1>Quản Lí Sản Phẩm</h1>
     </center>
	<br>
	<br>
	<br>

	<br>
	<div class="container">
      <a href="products_add.php">Thêm sản phẩm</a>
      <br>
      <br>
      <table border="1px" class="text-center">
      	  <tr>
      	  	<td>
      	  		Id Sản Phẩm
      	  	</td>
      	  	<td>
      	  		Mã sản phẩm
      	  	</td>
      	  	<td>
      	  		Hình ảnh
      	  	</td>
      	  	<td>
      	  		Tên sản phẩm
      	  	</td>
      	  	<td>
      	  		Nhãn hiệu
      	  	</td>
      	  	<td>
      	  		Đơn giá
      	  	</td>
      	  	<td>
      	  		Số lượng còn lại
      	  	</td>
      	  	<td>
      	  		Đánh giá
      	  	</td>
      	  	<td>
      	  		Trạng thái
      	  	</td>
      	  	<td>Edit</td>
      	  </tr>
           <?php 
              
                $sql = "SELECT * FROM products";         
                $query = $db->query($sql);
                $result = $query->fetch_all(MYSQLI_ASSOC);
                foreach ($result as $product) { ?>
            <tr>
            	<td><?php echo $product['id']; ?></td>
            	<td><?php echo $product['sku']; ?></td>
            	<td><?php?></td>
            	<td><?php echo $product['name']; ?></td>
            	<td><?php 
			                $sql = "SELECT * FROM brands";
			                $query = $db->query($sql);
			                $result = $query->fetch_all(MYSQLI_ASSOC); 
				             foreach ($result as $brand ) {
				             	if ($brand['id'] == $product['brand_id']) {
				             	   echo $brand['name'] . "<br>";
				             	}   
                             }
            	?></td>
            	<td><?php echo $product['price']; ?></td>
            	<td><?php echo $product['qty']; ?></td>
            	<td><?php echo $product['rank']; ?></td>
            	<td>
                <p>
                  <?php  if ($product['status'] == 1) {
                        		    echo "Đang treo";
                        	} else {
                                echo "Hủy treo";
                          } ?>	
                 </p>
                 <p>
                   <?php  if ($product['is_new'] == 1) {
                                echo "Sản Phẩm Mới";
                          } else {
                                echo "Sản Phẩm Cũ";
                          } ?>
                 </p>	
                 <p>
                   <?php  if ($product['is_promo'] == 1) {
                                echo "Sản Phẩm Quảng Cáo";
                          } else {
                                echo "Sản Phẩm Không Quảng Cáo";
                          } ?>
                 </p> 
                  <p>
                   <?php  if ($product['is_featured'] == 1) {
                                echo "Sản Phẩm Quảng Cáo";
                          } else {
                                echo "Sản Phẩm Không Quảng Cáo";
                          } ?>
                 </p> 
            	</td>
                <td>
                	  <p><a href="products_edit.php?id=<?php echo $product['id'] ?>">Cập nhật thông tin</a></p>
                     <p><a href="products_delete.php?id=<?php echo $product['id'] ?>">Hủy Kinh Doanh</a></p>
                      <p>

                           <a href="products_status.php?id=<?php echo $product['id'] ?>">Điều chỉnh trạng thái</a>     
                      </p>
                </td>
            </tr>    
            <?php    
                }
            ?>
      </table>
     </div>
    <script src="js/jquery-3.2.1.min.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script>	
</body>
</html>