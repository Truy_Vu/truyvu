-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 17, 2018 at 09:15 AM
-- Server version: 10.1.29-MariaDB
-- PHP Version: 7.1.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: ` business`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `pass` varchar(255) NOT NULL,
  `fullname` varchar(255) NOT NULL,
  `status` int(11) NOT NULL,
  `roles` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `brands`
--

CREATE TABLE `brands` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `logo` int(11) NOT NULL,
  `image` int(11) NOT NULL,
  `description` int(11) NOT NULL,
  `status` int(1) NOT NULL,
  `meta_descripition` varchar(255) NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_key_word` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `brands`
--

INSERT INTO `brands` (`id`, `name`, `slug`, `logo`, `image`, `description`, `status`, `meta_descripition`, `meta_title`, `meta_key_word`) VALUES
(1, 'Converse', 'Converse', 0, 0, 0, 1, '', '', ''),
(2, 'Alexander McQueen', 'Alexander McQueen', 0, 0, 0, 1, '', '', ''),
(3, 'Balenciaga', 'Balenciaga', 0, 0, 0, 1, '', '', ''),
(4, 'Nike', 'Nike', 0, 0, 0, 1, '', '', ''),
(5, 'Adidas', 'Adidas', 0, 0, 0, 1, '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `status` int(1) NOT NULL,
  `parrent_id` int(11) NOT NULL,
  `image` int(11) NOT NULL,
  `desciption` varchar(255) NOT NULL,
  `meta_descripition` varchar(255) NOT NULL,
  `meta_title` varchar(2555) NOT NULL,
  `meta_key_word` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `fullname` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `tel` int(11) NOT NULL,
  `content` text NOT NULL,
  `created_at` date NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `coupons`
--

CREATE TABLE `coupons` (
  `id` int(11) NOT NULL,
  `code` varchar(50) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `amount` float NOT NULL,
  `type` varchar(255) NOT NULL,
  `use_per_user` int(11) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `tel` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `sex` int(11) NOT NULL,
  `pass` varchar(255) NOT NULL,
  `customer_group_id` int(11) NOT NULL,
  `province_id` int(11) NOT NULL,
  `district_id` int(11) NOT NULL,
  `created_at` date NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `customer_groups`
--

CREATE TABLE `customer_groups` (
  `id` int(11) NOT NULL,
  `custom_id` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `amount` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `districts`
--

CREATE TABLE `districts` (
  `id` int(11) NOT NULL,
  `province_code` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `districts`
--

INSERT INTO `districts` (`id`, `province_code`, `name`, `type`) VALUES
(1, 1, 'Quận Ba Đình', 'Quận'),
(2, 1, 'Quận Hoàn Kiếm', 'Quận'),
(3, 1, 'Quận Tây Hồ', 'Quận'),
(4, 1, 'Quận Long Biên', 'Quận'),
(5, 1, 'Quận Cầu Giấy', 'Quận'),
(6, 1, 'Quận Đống Đa', 'Quận'),
(7, 1, 'Quận Hai Bà Trưng', 'Quận'),
(8, 1, 'Quận Hoàng Mai', 'Quận'),
(9, 1, 'Quận Thanh Xuân', 'Quận'),
(16, 1, 'Huyện Sóc Sơn', 'Huyện'),
(17, 1, 'Huyện Đông Anh', 'Huyện'),
(18, 1, 'Huyện Gia Lâm', 'Huyện'),
(19, 1, 'Quận Nam Từ Liêm', 'Quận'),
(20, 1, 'Huyện Thanh Trì', 'Huyện'),
(21, 1, 'Quận Bắc Từ Liêm', 'Quận'),
(24, 2, 'Thành phố Hà Giang', 'Thành phố'),
(26, 2, 'Huyện Đồng Văn', 'Huyện'),
(27, 2, 'Huyện Mèo Vạc', 'Huyện'),
(28, 2, 'Huyện Yên Minh', 'Huyện'),
(29, 2, 'Huyện Quản Bạ', 'Huyện'),
(30, 2, 'Huyện Vị Xuyên', 'Huyện'),
(31, 2, 'Huyện Bắc Mê', 'Huyện'),
(32, 2, 'Huyện Hoàng Su Phì', 'Huyện'),
(33, 2, 'Huyện Xín Mần', 'Huyện'),
(34, 2, 'Huyện Bắc Quang', 'Huyện'),
(35, 2, 'Huyện Quang Bình', 'Huyện'),
(40, 4, 'Thành phố Cao Bằng', 'Thành phố'),
(42, 4, 'Huyện Bảo Lâm', 'Huyện'),
(43, 4, 'Huyện Bảo Lạc', 'Huyện'),
(44, 4, 'Huyện Thông Nông', 'Huyện'),
(45, 4, 'Huyện Hà Quảng', 'Huyện'),
(46, 4, 'Huyện Trà Lĩnh', 'Huyện'),
(47, 4, 'Huyện Trùng Khánh', 'Huyện'),
(48, 4, 'Huyện Hạ Lang', 'Huyện'),
(49, 4, 'Huyện Quảng Uyên', 'Huyện'),
(50, 4, 'Huyện Phục Hoà', 'Huyện'),
(51, 4, 'Huyện Hoà An', 'Huyện'),
(52, 4, 'Huyện Nguyên Bình', 'Huyện'),
(53, 4, 'Huyện Thạch An', 'Huyện'),
(58, 6, 'Thành Phố Bắc Kạn', 'Thành phố'),
(60, 6, 'Huyện Pác Nặm', 'Huyện'),
(61, 6, 'Huyện Ba Bể', 'Huyện'),
(62, 6, 'Huyện Ngân Sơn', 'Huyện'),
(63, 6, 'Huyện Bạch Thông', 'Huyện'),
(64, 6, 'Huyện Chợ Đồn', 'Huyện'),
(65, 6, 'Huyện Chợ Mới', 'Huyện'),
(66, 6, 'Huyện Na Rì', 'Huyện'),
(70, 8, 'Thành phố Tuyên Quang', 'Thành phố'),
(71, 8, 'Huyện Lâm Bình', 'Huyện'),
(72, 8, 'Huyện Nà Hang', 'Huyện'),
(73, 8, 'Huyện Chiêm Hóa', 'Huyện'),
(74, 8, 'Huyện Hàm Yên', 'Huyện'),
(75, 8, 'Huyện Yên Sơn', 'Huyện'),
(76, 8, 'Huyện Sơn Dương', 'Huyện'),
(80, 10, 'Thành phố Lào Cai', 'Thành phố'),
(82, 10, 'Huyện Bát Xát', 'Huyện'),
(83, 10, 'Huyện Mường Khương', 'Huyện'),
(84, 10, 'Huyện Si Ma Cai', 'Huyện'),
(85, 10, 'Huyện Bắc Hà', 'Huyện'),
(86, 10, 'Huyện Bảo Thắng', 'Huyện'),
(87, 10, 'Huyện Bảo Yên', 'Huyện'),
(88, 10, 'Huyện Sa Pa', 'Huyện'),
(89, 10, 'Huyện Văn Bàn', 'Huyện'),
(94, 11, 'Thành phố Điện Biên Phủ', 'Thành phố'),
(95, 11, 'Thị Xã Mường Lay', 'Thị xã'),
(96, 11, 'Huyện Mường Nhé', 'Huyện'),
(97, 11, 'Huyện Mường Chà', 'Huyện'),
(98, 11, 'Huyện Tủa Chùa', 'Huyện'),
(99, 11, 'Huyện Tuần Giáo', 'Huyện'),
(100, 11, 'Huyện Điện Biên', 'Huyện'),
(101, 11, 'Huyện Điện Biên Đông', 'Huyện'),
(102, 11, 'Huyện Mường Ảng', 'Huyện'),
(103, 11, 'Huyện Nậm Pồ', 'Huyện'),
(105, 12, 'Thành phố Lai Châu', 'Thành phố'),
(106, 12, 'Huyện Tam Đường', 'Huyện'),
(107, 12, 'Huyện Mường Tè', 'Huyện'),
(108, 12, 'Huyện Sìn Hồ', 'Huyện'),
(109, 12, 'Huyện Phong Thổ', 'Huyện'),
(110, 12, 'Huyện Than Uyên', 'Huyện'),
(111, 12, 'Huyện Tân Uyên', 'Huyện'),
(112, 12, 'Huyện Nậm Nhùn', 'Huyện'),
(116, 14, 'Thành phố Sơn La', 'Thành phố'),
(118, 14, 'Huyện Quỳnh Nhai', 'Huyện'),
(119, 14, 'Huyện Thuận Châu', 'Huyện'),
(120, 14, 'Huyện Mường La', 'Huyện'),
(121, 14, 'Huyện Bắc Yên', 'Huyện'),
(122, 14, 'Huyện Phù Yên', 'Huyện'),
(123, 14, 'Huyện Mộc Châu', 'Huyện'),
(124, 14, 'Huyện Yên Châu', 'Huyện'),
(125, 14, 'Huyện Mai Sơn', 'Huyện'),
(126, 14, 'Huyện Sông Mã', 'Huyện'),
(127, 14, 'Huyện Sốp Cộp', 'Huyện'),
(128, 14, 'Huyện Vân Hồ', 'Huyện'),
(132, 15, 'Thành phố Yên Bái', 'Thành phố'),
(133, 15, 'Thị xã Nghĩa Lộ', 'Thị xã'),
(135, 15, 'Huyện Lục Yên', 'Huyện'),
(136, 15, 'Huyện Văn Yên', 'Huyện'),
(137, 15, 'Huyện Mù Căng Chải', 'Huyện'),
(138, 15, 'Huyện Trấn Yên', 'Huyện'),
(139, 15, 'Huyện Trạm Tấu', 'Huyện'),
(140, 15, 'Huyện Văn Chấn', 'Huyện'),
(141, 15, 'Huyện Yên Bình', 'Huyện'),
(148, 17, 'Thành phố Hòa Bình', 'Thành phố'),
(150, 17, 'Huyện Đà Bắc', 'Huyện'),
(151, 17, 'Huyện Kỳ Sơn', 'Huyện'),
(152, 17, 'Huyện Lương Sơn', 'Huyện'),
(153, 17, 'Huyện Kim Bôi', 'Huyện'),
(154, 17, 'Huyện Cao Phong', 'Huyện'),
(155, 17, 'Huyện Tân Lạc', 'Huyện'),
(156, 17, 'Huyện Mai Châu', 'Huyện'),
(157, 17, 'Huyện Lạc Sơn', 'Huyện'),
(158, 17, 'Huyện Yên Thủy', 'Huyện'),
(159, 17, 'Huyện Lạc Thủy', 'Huyện'),
(164, 19, 'Thành phố Thái Nguyên', 'Thành phố'),
(165, 19, 'Thành phố Sông Công', 'Thành phố'),
(167, 19, 'Huyện Định Hóa', 'Huyện'),
(168, 19, 'Huyện Phú Lương', 'Huyện'),
(169, 19, 'Huyện Đồng Hỷ', 'Huyện'),
(170, 19, 'Huyện Võ Nhai', 'Huyện'),
(171, 19, 'Huyện Đại Từ', 'Huyện'),
(172, 19, 'Thị xã Phổ Yên', 'Thị xã'),
(173, 19, 'Huyện Phú Bình', 'Huyện'),
(178, 20, 'Thành phố Lạng Sơn', 'Thành phố'),
(180, 20, 'Huyện Tràng Định', 'Huyện'),
(181, 20, 'Huyện Bình Gia', 'Huyện'),
(182, 20, 'Huyện Văn Lãng', 'Huyện'),
(183, 20, 'Huyện Cao Lộc', 'Huyện'),
(184, 20, 'Huyện Văn Quan', 'Huyện'),
(185, 20, 'Huyện Bắc Sơn', 'Huyện'),
(186, 20, 'Huyện Hữu Lũng', 'Huyện'),
(187, 20, 'Huyện Chi Lăng', 'Huyện'),
(188, 20, 'Huyện Lộc Bình', 'Huyện'),
(189, 20, 'Huyện Đình Lập', 'Huyện'),
(193, 22, 'Thành phố Hạ Long', 'Thành phố'),
(194, 22, 'Thành phố Móng Cái', 'Thành phố'),
(195, 22, 'Thành phố Cẩm Phả', 'Thành phố'),
(196, 22, 'Thành phố Uông Bí', 'Thành phố'),
(198, 22, 'Huyện Bình Liêu', 'Huyện'),
(199, 22, 'Huyện Tiên Yên', 'Huyện'),
(200, 22, 'Huyện Đầm Hà', 'Huyện'),
(201, 22, 'Huyện Hải Hà', 'Huyện'),
(202, 22, 'Huyện Ba Chẽ', 'Huyện'),
(203, 22, 'Huyện Vân Đồn', 'Huyện'),
(204, 22, 'Huyện Hoành Bồ', 'Huyện'),
(205, 22, 'Thị xã Đông Triều', 'Thị xã'),
(206, 22, 'Thị xã Quảng Yên', 'Thị xã'),
(207, 22, 'Huyện Cô Tô', 'Huyện'),
(213, 24, 'Thành phố Bắc Giang', 'Thành phố'),
(215, 24, 'Huyện Yên Thế', 'Huyện'),
(216, 24, 'Huyện Tân Yên', 'Huyện'),
(217, 24, 'Huyện Lạng Giang', 'Huyện'),
(218, 24, 'Huyện Lục Nam', 'Huyện'),
(219, 24, 'Huyện Lục Ngạn', 'Huyện'),
(220, 24, 'Huyện Sơn Động', 'Huyện'),
(221, 24, 'Huyện Yên Dũng', 'Huyện'),
(222, 24, 'Huyện Việt Yên', 'Huyện'),
(223, 24, 'Huyện Hiệp Hòa', 'Huyện'),
(227, 25, 'Thành phố Việt Trì', 'Thành phố'),
(228, 25, 'Thị xã Phú Thọ', 'Thị xã'),
(230, 25, 'Huyện Đoan Hùng', 'Huyện'),
(231, 25, 'Huyện Hạ Hoà', 'Huyện'),
(232, 25, 'Huyện Thanh Ba', 'Huyện'),
(233, 25, 'Huyện Phù Ninh', 'Huyện'),
(234, 25, 'Huyện Yên Lập', 'Huyện'),
(235, 25, 'Huyện Cẩm Khê', 'Huyện'),
(236, 25, 'Huyện Tam Nông', 'Huyện'),
(237, 25, 'Huyện Lâm Thao', 'Huyện'),
(238, 25, 'Huyện Thanh Sơn', 'Huyện'),
(239, 25, 'Huyện Thanh Thuỷ', 'Huyện'),
(240, 25, 'Huyện Tân Sơn', 'Huyện'),
(243, 26, 'Thành phố Vĩnh Yên', 'Thành phố'),
(244, 26, 'Thị xã Phúc Yên', 'Thị xã'),
(246, 26, 'Huyện Lập Thạch', 'Huyện'),
(247, 26, 'Huyện Tam Dương', 'Huyện'),
(248, 26, 'Huyện Tam Đảo', 'Huyện'),
(249, 26, 'Huyện Bình Xuyên', 'Huyện'),
(250, 1, 'Huyện Mê Linh', 'Huyện'),
(251, 26, 'Huyện Yên Lạc', 'Huyện'),
(252, 26, 'Huyện Vĩnh Tường', 'Huyện'),
(253, 26, 'Huyện Sông Lô', 'Huyện'),
(256, 27, 'Thành phố Bắc Ninh', 'Thành phố'),
(258, 27, 'Huyện Yên Phong', 'Huyện'),
(259, 27, 'Huyện Quế Võ', 'Huyện'),
(260, 27, 'Huyện Tiên Du', 'Huyện'),
(261, 27, 'Thị xã Từ Sơn', 'Thị xã'),
(262, 27, 'Huyện Thuận Thành', 'Huyện'),
(263, 27, 'Huyện Gia Bình', 'Huyện'),
(264, 27, 'Huyện Lương Tài', 'Huyện'),
(268, 1, 'Quận Hà Đông', 'Quận'),
(269, 1, 'Thị xã Sơn Tây', 'Thị xã'),
(271, 1, 'Huyện Ba Vì', 'Huyện'),
(272, 1, 'Huyện Phúc Thọ', 'Huyện'),
(273, 1, 'Huyện Đan Phượng', 'Huyện'),
(274, 1, 'Huyện Hoài Đức', 'Huyện'),
(275, 1, 'Huyện Quốc Oai', 'Huyện'),
(276, 1, 'Huyện Thạch Thất', 'Huyện'),
(277, 1, 'Huyện Chương Mỹ', 'Huyện'),
(278, 1, 'Huyện Thanh Oai', 'Huyện'),
(279, 1, 'Huyện Thường Tín', 'Huyện'),
(280, 1, 'Huyện Phú Xuyên', 'Huyện'),
(281, 1, 'Huyện Ứng Hòa', 'Huyện'),
(282, 1, 'Huyện Mỹ Đức', 'Huyện'),
(288, 30, 'Thành phố Hải Dương', 'Thành phố'),
(290, 30, 'Thị xã Chí Linh', 'Thị xã'),
(291, 30, 'Huyện Nam Sách', 'Huyện'),
(292, 30, 'Huyện Kinh Môn', 'Huyện'),
(293, 30, 'Huyện Kim Thành', 'Huyện'),
(294, 30, 'Huyện Thanh Hà', 'Huyện'),
(295, 30, 'Huyện Cẩm Giàng', 'Huyện'),
(296, 30, 'Huyện Bình Giang', 'Huyện'),
(297, 30, 'Huyện Gia Lộc', 'Huyện'),
(298, 30, 'Huyện Tứ Kỳ', 'Huyện'),
(299, 30, 'Huyện Ninh Giang', 'Huyện'),
(300, 30, 'Huyện Thanh Miện', 'Huyện'),
(303, 31, 'Quận Hồng Bàng', 'Quận'),
(304, 31, 'Quận Ngô Quyền', 'Quận'),
(305, 31, 'Quận Lê Chân', 'Quận'),
(306, 31, 'Quận Hải An', 'Quận'),
(307, 31, 'Quận Kiến An', 'Quận'),
(308, 31, 'Quận Đồ Sơn', 'Quận'),
(309, 31, 'Quận Dương Kinh', 'Quận'),
(311, 31, 'Huyện Thuỷ Nguyên', 'Huyện'),
(312, 31, 'Huyện An Dương', 'Huyện'),
(313, 31, 'Huyện An Lão', 'Huyện'),
(314, 31, 'Huyện Kiến Thuỵ', 'Huyện'),
(315, 31, 'Huyện Tiên Lãng', 'Huyện'),
(316, 31, 'Huyện Vĩnh Bảo', 'Huyện'),
(317, 31, 'Huyện Cát Hải', 'Huyện'),
(318, 31, 'Huyện Bạch Long Vĩ', 'Huyện'),
(323, 33, 'Thành phố Hưng Yên', 'Thành phố'),
(325, 33, 'Huyện Văn Lâm', 'Huyện'),
(326, 33, 'Huyện Văn Giang', 'Huyện'),
(327, 33, 'Huyện Yên Mỹ', 'Huyện'),
(328, 33, 'Huyện Mỹ Hào', 'Huyện'),
(329, 33, 'Huyện Ân Thi', 'Huyện'),
(330, 33, 'Huyện Khoái Châu', 'Huyện'),
(331, 33, 'Huyện Kim Động', 'Huyện'),
(332, 33, 'Huyện Tiên Lữ', 'Huyện'),
(333, 33, 'Huyện Phù Cừ', 'Huyện'),
(336, 34, 'Thành phố Thái Bình', 'Thành phố'),
(338, 34, 'Huyện Quỳnh Phụ', 'Huyện'),
(339, 34, 'Huyện Hưng Hà', 'Huyện'),
(340, 34, 'Huyện Đông Hưng', 'Huyện'),
(341, 34, 'Huyện Thái Thụy', 'Huyện'),
(342, 34, 'Huyện Tiền Hải', 'Huyện'),
(343, 34, 'Huyện Kiến Xương', 'Huyện'),
(344, 34, 'Huyện Vũ Thư', 'Huyện'),
(347, 35, 'Thành phố Phủ Lý', 'Thành phố'),
(349, 35, 'Huyện Duy Tiên', 'Huyện'),
(350, 35, 'Huyện Kim Bảng', 'Huyện'),
(351, 35, 'Huyện Thanh Liêm', 'Huyện'),
(352, 35, 'Huyện Bình Lục', 'Huyện'),
(353, 35, 'Huyện Lý Nhân', 'Huyện'),
(356, 36, 'Thành phố Nam Định', 'Thành phố'),
(358, 36, 'Huyện Mỹ Lộc', 'Huyện'),
(359, 36, 'Huyện Vụ Bản', 'Huyện'),
(360, 36, 'Huyện Ý Yên', 'Huyện'),
(361, 36, 'Huyện Nghĩa Hưng', 'Huyện'),
(362, 36, 'Huyện Nam Trực', 'Huyện'),
(363, 36, 'Huyện Trực Ninh', 'Huyện'),
(364, 36, 'Huyện Xuân Trường', 'Huyện'),
(365, 36, 'Huyện Giao Thủy', 'Huyện'),
(366, 36, 'Huyện Hải Hậu', 'Huyện'),
(369, 37, 'Thành phố Ninh Bình', 'Thành phố'),
(370, 37, 'Thành phố Tam Điệp', 'Thành phố'),
(372, 37, 'Huyện Nho Quan', 'Huyện'),
(373, 37, 'Huyện Gia Viễn', 'Huyện'),
(374, 37, 'Huyện Hoa Lư', 'Huyện'),
(375, 37, 'Huyện Yên Khánh', 'Huyện'),
(376, 37, 'Huyện Kim Sơn', 'Huyện'),
(377, 37, 'Huyện Yên Mô', 'Huyện'),
(380, 38, 'Thành phố Thanh Hóa', 'Thành phố'),
(381, 38, 'Thị xã Bỉm Sơn', 'Thị xã'),
(382, 38, 'Thị xã Sầm Sơn', 'Thị xã'),
(384, 38, 'Huyện Mường Lát', 'Huyện'),
(385, 38, 'Huyện Quan Hóa', 'Huyện'),
(386, 38, 'Huyện Bá Thước', 'Huyện'),
(387, 38, 'Huyện Quan Sơn', 'Huyện'),
(388, 38, 'Huyện Lang Chánh', 'Huyện'),
(389, 38, 'Huyện Ngọc Lặc', 'Huyện'),
(390, 38, 'Huyện Cẩm Thủy', 'Huyện'),
(391, 38, 'Huyện Thạch Thành', 'Huyện'),
(392, 38, 'Huyện Hà Trung', 'Huyện'),
(393, 38, 'Huyện Vĩnh Lộc', 'Huyện'),
(394, 38, 'Huyện Yên Định', 'Huyện'),
(395, 38, 'Huyện Thọ Xuân', 'Huyện'),
(396, 38, 'Huyện Thường Xuân', 'Huyện'),
(397, 38, 'Huyện Triệu Sơn', 'Huyện'),
(398, 38, 'Huyện Thiệu Hóa', 'Huyện'),
(399, 38, 'Huyện Hoằng Hóa', 'Huyện'),
(400, 38, 'Huyện Hậu Lộc', 'Huyện'),
(401, 38, 'Huyện Nga Sơn', 'Huyện'),
(402, 38, 'Huyện Như Xuân', 'Huyện'),
(403, 38, 'Huyện Như Thanh', 'Huyện'),
(404, 38, 'Huyện Nông Cống', 'Huyện'),
(405, 38, 'Huyện Đông Sơn', 'Huyện'),
(406, 38, 'Huyện Quảng Xương', 'Huyện'),
(407, 38, 'Huyện Tĩnh Gia', 'Huyện'),
(412, 40, 'Thành phố Vinh', 'Thành phố'),
(413, 40, 'Thị xã Cửa Lò', 'Thị xã'),
(414, 40, 'Thị xã Thái Hoà', 'Thị xã'),
(415, 40, 'Huyện Quế Phong', 'Huyện'),
(416, 40, 'Huyện Quỳ Châu', 'Huyện'),
(417, 40, 'Huyện Kỳ Sơn', 'Huyện'),
(418, 40, 'Huyện Tương Dương', 'Huyện'),
(419, 40, 'Huyện Nghĩa Đàn', 'Huyện'),
(420, 40, 'Huyện Quỳ Hợp', 'Huyện'),
(421, 40, 'Huyện Quỳnh Lưu', 'Huyện'),
(422, 40, 'Huyện Con Cuông', 'Huyện'),
(423, 40, 'Huyện Tân Kỳ', 'Huyện'),
(424, 40, 'Huyện Anh Sơn', 'Huyện'),
(425, 40, 'Huyện Diễn Châu', 'Huyện'),
(426, 40, 'Huyện Yên Thành', 'Huyện'),
(427, 40, 'Huyện Đô Lương', 'Huyện'),
(428, 40, 'Huyện Thanh Chương', 'Huyện'),
(429, 40, 'Huyện Nghi Lộc', 'Huyện'),
(430, 40, 'Huyện Nam Đàn', 'Huyện'),
(431, 40, 'Huyện Hưng Nguyên', 'Huyện'),
(432, 40, 'Thị xã Hoàng Mai', 'Thị xã'),
(436, 42, 'Thành phố Hà Tĩnh', 'Thành phố'),
(437, 42, 'Thị xã Hồng Lĩnh', 'Thị xã'),
(439, 42, 'Huyện Hương Sơn', 'Huyện'),
(440, 42, 'Huyện Đức Thọ', 'Huyện'),
(441, 42, 'Huyện Vũ Quang', 'Huyện'),
(442, 42, 'Huyện Nghi Xuân', 'Huyện'),
(443, 42, 'Huyện Can Lộc', 'Huyện'),
(444, 42, 'Huyện Hương Khê', 'Huyện'),
(445, 42, 'Huyện Thạch Hà', 'Huyện'),
(446, 42, 'Huyện Cẩm Xuyên', 'Huyện'),
(447, 42, 'Huyện Kỳ Anh', 'Huyện'),
(448, 42, 'Huyện Lộc Hà', 'Huyện'),
(449, 42, 'Thị xã Kỳ Anh', 'Thị xã'),
(450, 44, 'Thành Phố Đồng Hới', 'Thành phố'),
(452, 44, 'Huyện Minh Hóa', 'Huyện'),
(453, 44, 'Huyện Tuyên Hóa', 'Huyện'),
(454, 44, 'Huyện Quảng Trạch', 'Thị xã'),
(455, 44, 'Huyện Bố Trạch', 'Huyện'),
(456, 44, 'Huyện Quảng Ninh', 'Huyện'),
(457, 44, 'Huyện Lệ Thủy', 'Huyện'),
(458, 44, 'Thị xã Ba Đồn', 'Huyện'),
(461, 45, 'Thành phố Đông Hà', 'Thành phố'),
(462, 45, 'Thị xã Quảng Trị', 'Thị xã'),
(464, 45, 'Huyện Vĩnh Linh', 'Huyện'),
(465, 45, 'Huyện Hướng Hóa', 'Huyện'),
(466, 45, 'Huyện Gio Linh', 'Huyện'),
(467, 45, 'Huyện Đa Krông', 'Huyện'),
(468, 45, 'Huyện Cam Lộ', 'Huyện'),
(469, 45, 'Huyện Triệu Phong', 'Huyện'),
(470, 45, 'Huyện Hải Lăng', 'Huyện'),
(471, 45, 'Huyện Cồn Cỏ', 'Huyện'),
(474, 46, 'Thành phố Huế', 'Thành phố'),
(476, 46, 'Huyện Phong Điền', 'Huyện'),
(477, 46, 'Huyện Quảng Điền', 'Huyện'),
(478, 46, 'Huyện Phú Vang', 'Huyện'),
(479, 46, 'Thị xã Hương Thủy', 'Thị xã'),
(480, 46, 'Thị xã Hương Trà', 'Thị xã'),
(481, 46, 'Huyện A Lưới', 'Huyện'),
(482, 46, 'Huyện Phú Lộc', 'Huyện'),
(483, 46, 'Huyện Nam Đông', 'Huyện'),
(490, 48, 'Quận Liên Chiểu', 'Quận'),
(491, 48, 'Quận Thanh Khê', 'Quận'),
(492, 48, 'Quận Hải Châu', 'Quận'),
(493, 48, 'Quận Sơn Trà', 'Quận'),
(494, 48, 'Quận Ngũ Hành Sơn', 'Quận'),
(495, 48, 'Quận Cẩm Lệ', 'Quận'),
(497, 48, 'Huyện Hòa Vang', 'Huyện'),
(498, 48, 'Huyện Hoàng Sa', 'Huyện'),
(502, 49, 'Thành phố Tam Kỳ', 'Thành phố'),
(503, 49, 'Thành phố Hội An', 'Thành phố'),
(504, 49, 'Huyện Tây Giang', 'Huyện'),
(505, 49, 'Huyện Đông Giang', 'Huyện'),
(506, 49, 'Huyện Đại Lộc', 'Huyện'),
(507, 49, 'Thị xã Điện Bàn', 'Thị xã'),
(508, 49, 'Huyện Duy Xuyên', 'Huyện'),
(509, 49, 'Huyện Quế Sơn', 'Huyện'),
(510, 49, 'Huyện Nam Giang', 'Huyện'),
(511, 49, 'Huyện Phước Sơn', 'Huyện'),
(512, 49, 'Huyện Hiệp Đức', 'Huyện'),
(513, 49, 'Huyện Thăng Bình', 'Huyện'),
(514, 49, 'Huyện Tiên Phước', 'Huyện'),
(515, 49, 'Huyện Bắc Trà My', 'Huyện'),
(516, 49, 'Huyện Nam Trà My', 'Huyện'),
(517, 49, 'Huyện Núi Thành', 'Huyện'),
(518, 49, 'Huyện Phú Ninh', 'Huyện'),
(519, 49, 'Huyện Nông Sơn', 'Huyện'),
(522, 51, 'Thành phố Quảng Ngãi', 'Thành phố'),
(524, 51, 'Huyện Bình Sơn', 'Huyện'),
(525, 51, 'Huyện Trà Bồng', 'Huyện'),
(526, 51, 'Huyện Tây Trà', 'Huyện'),
(527, 51, 'Huyện Sơn Tịnh', 'Huyện'),
(528, 51, 'Huyện Tư Nghĩa', 'Huyện'),
(529, 51, 'Huyện Sơn Hà', 'Huyện'),
(530, 51, 'Huyện Sơn Tây', 'Huyện'),
(531, 51, 'Huyện Minh Long', 'Huyện'),
(532, 51, 'Huyện Nghĩa Hành', 'Huyện'),
(533, 51, 'Huyện Mộ Đức', 'Huyện'),
(534, 51, 'Huyện Đức Phổ', 'Huyện'),
(535, 51, 'Huyện Ba Tơ', 'Huyện'),
(536, 51, 'Huyện Lý Sơn', 'Huyện'),
(540, 52, 'Thành phố Qui Nhơn', 'Thành phố'),
(542, 52, 'Huyện An Lão', 'Huyện'),
(543, 52, 'Huyện Hoài Nhơn', 'Huyện'),
(544, 52, 'Huyện Hoài Ân', 'Huyện'),
(545, 52, 'Huyện Phù Mỹ', 'Huyện'),
(546, 52, 'Huyện Vĩnh Thạnh', 'Huyện'),
(547, 52, 'Huyện Tây Sơn', 'Huyện'),
(548, 52, 'Huyện Phù Cát', 'Huyện'),
(549, 52, 'Thị xã An Nhơn', 'Thị xã'),
(550, 52, 'Huyện Tuy Phước', 'Huyện'),
(551, 52, 'Huyện Vân Canh', 'Huyện'),
(555, 54, 'Thành phố Tuy Hoà', 'Thành phố'),
(557, 54, 'Thị xã Sông Cầu', 'Thị xã'),
(558, 54, 'Huyện Đồng Xuân', 'Huyện'),
(559, 54, 'Huyện Tuy An', 'Huyện'),
(560, 54, 'Huyện Sơn Hòa', 'Huyện'),
(561, 54, 'Huyện Sông Hinh', 'Huyện'),
(562, 54, 'Huyện Tây Hoà', 'Huyện'),
(563, 54, 'Huyện Phú Hoà', 'Huyện'),
(564, 54, 'Huyện Đông Hòa', 'Huyện'),
(568, 56, 'Thành phố Nha Trang', 'Thành phố'),
(569, 56, 'Thành phố Cam Ranh', 'Thành phố'),
(570, 56, 'Huyện Cam Lâm', 'Huyện'),
(571, 56, 'Huyện Vạn Ninh', 'Huyện'),
(572, 56, 'Thị xã Ninh Hòa', 'Thị xã'),
(573, 56, 'Huyện Khánh Vĩnh', 'Huyện'),
(574, 56, 'Huyện Diên Khánh', 'Huyện'),
(575, 56, 'Huyện Khánh Sơn', 'Huyện'),
(576, 56, 'Huyện Trường Sa', 'Huyện'),
(582, 58, 'Thành phố Phan Rang-Tháp Chàm', 'Thành phố'),
(584, 58, 'Huyện Bác Ái', 'Huyện'),
(585, 58, 'Huyện Ninh Sơn', 'Huyện'),
(586, 58, 'Huyện Ninh Hải', 'Huyện'),
(587, 58, 'Huyện Ninh Phước', 'Huyện'),
(588, 58, 'Huyện Thuận Bắc', 'Huyện'),
(589, 58, 'Huyện Thuận Nam', 'Huyện'),
(593, 60, 'Thành phố Phan Thiết', 'Thành phố'),
(594, 60, 'Thị xã La Gi', 'Thị xã'),
(595, 60, 'Huyện Tuy Phong', 'Huyện'),
(596, 60, 'Huyện Bắc Bình', 'Huyện'),
(597, 60, 'Huyện Hàm Thuận Bắc', 'Huyện'),
(598, 60, 'Huyện Hàm Thuận Nam', 'Huyện'),
(599, 60, 'Huyện Tánh Linh', 'Huyện'),
(600, 60, 'Huyện Đức Linh', 'Huyện'),
(601, 60, 'Huyện Hàm Tân', 'Huyện'),
(602, 60, 'Huyện Phú Quí', 'Huyện'),
(608, 62, 'Thành phố Kon Tum', 'Thành phố'),
(610, 62, 'Huyện Đắk Glei', 'Huyện'),
(611, 62, 'Huyện Ngọc Hồi', 'Huyện'),
(612, 62, 'Huyện Đắk Tô', 'Huyện'),
(613, 62, 'Huyện Kon Plông', 'Huyện'),
(614, 62, 'Huyện Kon Rẫy', 'Huyện'),
(615, 62, 'Huyện Đắk Hà', 'Huyện'),
(616, 62, 'Huyện Sa Thầy', 'Huyện'),
(617, 62, 'Huyện Tu Mơ Rông', 'Huyện'),
(618, 62, 'Huyện Ia H\' Drai', 'Huyện'),
(622, 64, 'Thành phố Pleiku', 'Thành phố'),
(623, 64, 'Thị xã An Khê', 'Thị xã'),
(624, 64, 'Thị xã Ayun Pa', 'Thị xã'),
(625, 64, 'Huyện KBang', 'Huyện'),
(626, 64, 'Huyện Đăk Đoa', 'Huyện'),
(627, 64, 'Huyện Chư Păh', 'Huyện'),
(628, 64, 'Huyện Ia Grai', 'Huyện'),
(629, 64, 'Huyện Mang Yang', 'Huyện'),
(630, 64, 'Huyện Kông Chro', 'Huyện'),
(631, 64, 'Huyện Đức Cơ', 'Huyện'),
(632, 64, 'Huyện Chư Prông', 'Huyện'),
(633, 64, 'Huyện Chư Sê', 'Huyện'),
(634, 64, 'Huyện Đăk Pơ', 'Huyện'),
(635, 64, 'Huyện Ia Pa', 'Huyện'),
(637, 64, 'Huyện Krông Pa', 'Huyện'),
(638, 64, 'Huyện Phú Thiện', 'Huyện'),
(639, 64, 'Huyện Chư Pưh', 'Huyện'),
(643, 66, 'Thành phố Buôn Ma Thuột', 'Thành phố'),
(644, 66, 'Thị Xã Buôn Hồ', 'Thị xã'),
(645, 66, 'Huyện Ea H\'leo', 'Huyện'),
(646, 66, 'Huyện Ea Súp', 'Huyện'),
(647, 66, 'Huyện Buôn Đôn', 'Huyện'),
(648, 66, 'Huyện Cư M\'gar', 'Huyện'),
(649, 66, 'Huyện Krông Búk', 'Huyện'),
(650, 66, 'Huyện Krông Năng', 'Huyện'),
(651, 66, 'Huyện Ea Kar', 'Huyện'),
(652, 66, 'Huyện M\'Đrắk', 'Huyện'),
(653, 66, 'Huyện Krông Bông', 'Huyện'),
(654, 66, 'Huyện Krông Pắc', 'Huyện'),
(655, 66, 'Huyện Krông A Na', 'Huyện'),
(656, 66, 'Huyện Lắk', 'Huyện'),
(657, 66, 'Huyện Cư Kuin', 'Huyện'),
(660, 67, 'Thị xã Gia Nghĩa', 'Thị xã'),
(661, 67, 'Huyện Đăk Glong', 'Huyện'),
(662, 67, 'Huyện Cư Jút', 'Huyện'),
(663, 67, 'Huyện Đắk Mil', 'Huyện'),
(664, 67, 'Huyện Krông Nô', 'Huyện'),
(665, 67, 'Huyện Đắk Song', 'Huyện'),
(666, 67, 'Huyện Đắk R\'Lấp', 'Huyện'),
(667, 67, 'Huyện Tuy Đức', 'Huyện'),
(672, 68, 'Thành phố Đà Lạt', 'Thành phố'),
(673, 68, 'Thành phố Bảo Lộc', 'Thành phố'),
(674, 68, 'Huyện Đam Rông', 'Huyện'),
(675, 68, 'Huyện Lạc Dương', 'Huyện'),
(676, 68, 'Huyện Lâm Hà', 'Huyện'),
(677, 68, 'Huyện Đơn Dương', 'Huyện'),
(678, 68, 'Huyện Đức Trọng', 'Huyện'),
(679, 68, 'Huyện Di Linh', 'Huyện'),
(680, 68, 'Huyện Bảo Lâm', 'Huyện'),
(681, 68, 'Huyện Đạ Huoai', 'Huyện'),
(682, 68, 'Huyện Đạ Tẻh', 'Huyện'),
(683, 68, 'Huyện Cát Tiên', 'Huyện'),
(688, 70, 'Thị xã Phước Long', 'Thị xã'),
(689, 70, 'Thị xã Đồng Xoài', 'Thị xã'),
(690, 70, 'Thị xã Bình Long', 'Thị xã'),
(691, 70, 'Huyện Bù Gia Mập', 'Huyện'),
(692, 70, 'Huyện Lộc Ninh', 'Huyện'),
(693, 70, 'Huyện Bù Đốp', 'Huyện'),
(694, 70, 'Huyện Hớn Quản', 'Huyện'),
(695, 70, 'Huyện Đồng Phú', 'Huyện'),
(696, 70, 'Huyện Bù Đăng', 'Huyện'),
(697, 70, 'Huyện Chơn Thành', 'Huyện'),
(698, 70, 'Huyện Phú Riềng', 'Huyện'),
(703, 72, 'Thành phố Tây Ninh', 'Thành phố'),
(705, 72, 'Huyện Tân Biên', 'Huyện'),
(706, 72, 'Huyện Tân Châu', 'Huyện'),
(707, 72, 'Huyện Dương Minh Châu', 'Huyện'),
(708, 72, 'Huyện Châu Thành', 'Huyện'),
(709, 72, 'Huyện Hòa Thành', 'Huyện'),
(710, 72, 'Huyện Gò Dầu', 'Huyện'),
(711, 72, 'Huyện Bến Cầu', 'Huyện'),
(712, 72, 'Huyện Trảng Bàng', 'Huyện'),
(718, 74, 'Thành phố Thủ Dầu Một', 'Thành phố'),
(719, 74, 'Huyện Bàu Bàng', 'Huyện'),
(720, 74, 'Huyện Dầu Tiếng', 'Huyện'),
(721, 74, 'Thị xã Bến Cát', 'Thị xã'),
(722, 74, 'Huyện Phú Giáo', 'Huyện'),
(723, 74, 'Thị xã Tân Uyên', 'Thị xã'),
(724, 74, 'Thị xã Dĩ An', 'Thị xã'),
(725, 74, 'Thị xã Thuận An', 'Thị xã'),
(726, 74, 'Huyện Bắc Tân Uyên', 'Huyện'),
(731, 75, 'Thành phố Biên Hòa', 'Thành phố'),
(732, 75, 'Thị xã Long Khánh', 'Thị xã'),
(734, 75, 'Huyện Tân Phú', 'Huyện'),
(735, 75, 'Huyện Vĩnh Cửu', 'Huyện'),
(736, 75, 'Huyện Định Quán', 'Huyện'),
(737, 75, 'Huyện Trảng Bom', 'Huyện'),
(738, 75, 'Huyện Thống Nhất', 'Huyện'),
(739, 75, 'Huyện Cẩm Mỹ', 'Huyện'),
(740, 75, 'Huyện Long Thành', 'Huyện'),
(741, 75, 'Huyện Xuân Lộc', 'Huyện'),
(742, 75, 'Huyện Nhơn Trạch', 'Huyện'),
(747, 77, 'Thành phố Vũng Tàu', 'Thành phố'),
(748, 77, 'Thành phố Bà Rịa', 'Thành phố'),
(750, 77, 'Huyện Châu Đức', 'Huyện'),
(751, 77, 'Huyện Xuyên Mộc', 'Huyện'),
(752, 77, 'Huyện Long Điền', 'Huyện'),
(753, 77, 'Huyện Đất Đỏ', 'Huyện'),
(754, 77, 'Huyện Tân Thành', 'Huyện'),
(755, 77, 'Huyện Côn Đảo', 'Huyện'),
(760, 79, 'Quận 1', 'Quận'),
(761, 79, 'Quận 12', 'Quận'),
(762, 79, 'Quận Thủ Đức', 'Quận'),
(763, 79, 'Quận 9', 'Quận'),
(764, 79, 'Quận Gò Vấp', 'Quận'),
(765, 79, 'Quận Bình Thạnh', 'Quận'),
(766, 79, 'Quận Tân Bình', 'Quận'),
(767, 79, 'Quận Tân Phú', 'Quận'),
(768, 79, 'Quận Phú Nhuận', 'Quận'),
(769, 79, 'Quận 2', 'Quận'),
(770, 79, 'Quận 3', 'Quận'),
(771, 79, 'Quận 10', 'Quận'),
(772, 79, 'Quận 11', 'Quận'),
(773, 79, 'Quận 4', 'Quận'),
(774, 79, 'Quận 5', 'Quận'),
(775, 79, 'Quận 6', 'Quận'),
(776, 79, 'Quận 8', 'Quận'),
(777, 79, 'Quận Bình Tân', 'Quận'),
(778, 79, 'Quận 7', 'Quận'),
(783, 79, 'Huyện Củ Chi', 'Huyện'),
(784, 79, 'Huyện Hóc Môn', 'Huyện'),
(785, 79, 'Huyện Bình Chánh', 'Huyện'),
(786, 79, 'Huyện Nhà Bè', 'Huyện'),
(787, 79, 'Huyện Cần Giờ', 'Huyện'),
(794, 80, 'Thành phố Tân An', 'Thành phố'),
(795, 80, 'Thị xã Kiến Tường', 'Thị xã'),
(796, 80, 'Huyện Tân Hưng', 'Huyện'),
(797, 80, 'Huyện Vĩnh Hưng', 'Huyện'),
(798, 80, 'Huyện Mộc Hóa', 'Huyện'),
(799, 80, 'Huyện Tân Thạnh', 'Huyện'),
(800, 80, 'Huyện Thạnh Hóa', 'Huyện'),
(801, 80, 'Huyện Đức Huệ', 'Huyện'),
(802, 80, 'Huyện Đức Hòa', 'Huyện'),
(803, 80, 'Huyện Bến Lức', 'Huyện'),
(804, 80, 'Huyện Thủ Thừa', 'Huyện'),
(805, 80, 'Huyện Tân Trụ', 'Huyện'),
(806, 80, 'Huyện Cần Đước', 'Huyện'),
(807, 80, 'Huyện Cần Giuộc', 'Huyện'),
(808, 80, 'Huyện Châu Thành', 'Huyện'),
(815, 82, 'Thành phố Mỹ Tho', 'Thành phố'),
(816, 82, 'Thị xã Gò Công', 'Thị xã'),
(817, 82, 'Thị xã Cai Lậy', 'Huyện'),
(818, 82, 'Huyện Tân Phước', 'Huyện'),
(819, 82, 'Huyện Cái Bè', 'Huyện'),
(820, 82, 'Huyện Cai Lậy', 'Thị xã'),
(821, 82, 'Huyện Châu Thành', 'Huyện'),
(822, 82, 'Huyện Chợ Gạo', 'Huyện'),
(823, 82, 'Huyện Gò Công Tây', 'Huyện'),
(824, 82, 'Huyện Gò Công Đông', 'Huyện'),
(825, 82, 'Huyện Tân Phú Đông', 'Huyện'),
(829, 83, 'Thành phố Bến Tre', 'Thành phố'),
(831, 83, 'Huyện Châu Thành', 'Huyện'),
(832, 83, 'Huyện Chợ Lách', 'Huyện'),
(833, 83, 'Huyện Mỏ Cày Nam', 'Huyện'),
(834, 83, 'Huyện Giồng Trôm', 'Huyện'),
(835, 83, 'Huyện Bình Đại', 'Huyện'),
(836, 83, 'Huyện Ba Tri', 'Huyện'),
(837, 83, 'Huyện Thạnh Phú', 'Huyện'),
(838, 83, 'Huyện Mỏ Cày Bắc', 'Huyện'),
(842, 84, 'Thành phố Trà Vinh', 'Thành phố'),
(844, 84, 'Huyện Càng Long', 'Huyện'),
(845, 84, 'Huyện Cầu Kè', 'Huyện'),
(846, 84, 'Huyện Tiểu Cần', 'Huyện'),
(847, 84, 'Huyện Châu Thành', 'Huyện'),
(848, 84, 'Huyện Cầu Ngang', 'Huyện'),
(849, 84, 'Huyện Trà Cú', 'Huyện'),
(850, 84, 'Huyện Duyên Hải', 'Huyện'),
(851, 84, 'Thị xã Duyên Hải', 'Thị xã'),
(855, 86, 'Thành phố Vĩnh Long', 'Thành phố'),
(857, 86, 'Huyện Long Hồ', 'Huyện'),
(858, 86, 'Huyện Mang Thít', 'Huyện'),
(859, 86, 'Huyện  Vũng Liêm', 'Huyện'),
(860, 86, 'Huyện Tam Bình', 'Huyện'),
(861, 86, 'Thị xã Bình Minh', 'Thị xã'),
(862, 86, 'Huyện Trà Ôn', 'Huyện'),
(863, 86, 'Huyện Bình Tân', 'Huyện'),
(866, 87, 'Thành phố Cao Lãnh', 'Thành phố'),
(867, 87, 'Thành phố Sa Đéc', 'Thành phố'),
(868, 87, 'Thị xã Hồng Ngự', 'Thị xã'),
(869, 87, 'Huyện Tân Hồng', 'Huyện'),
(870, 87, 'Huyện Hồng Ngự', 'Huyện'),
(871, 87, 'Huyện Tam Nông', 'Huyện'),
(872, 87, 'Huyện Tháp Mười', 'Huyện'),
(873, 87, 'Huyện Cao Lãnh', 'Huyện'),
(874, 87, 'Huyện Thanh Bình', 'Huyện'),
(875, 87, 'Huyện Lấp Vò', 'Huyện'),
(876, 87, 'Huyện Lai Vung', 'Huyện'),
(877, 87, 'Huyện Châu Thành', 'Huyện'),
(883, 89, 'Thành phố Long Xuyên', 'Thành phố'),
(884, 89, 'Thành phố Châu Đốc', 'Thành phố'),
(886, 89, 'Huyện An Phú', 'Huyện'),
(887, 89, 'Thị xã Tân Châu', 'Thị xã'),
(888, 89, 'Huyện Phú Tân', 'Huyện'),
(889, 89, 'Huyện Châu Phú', 'Huyện'),
(890, 89, 'Huyện Tịnh Biên', 'Huyện'),
(891, 89, 'Huyện Tri Tôn', 'Huyện'),
(892, 89, 'Huyện Châu Thành', 'Huyện'),
(893, 89, 'Huyện Chợ Mới', 'Huyện'),
(894, 89, 'Huyện Thoại Sơn', 'Huyện'),
(899, 91, 'Thành phố Rạch Giá', 'Thành phố'),
(900, 91, 'Thị xã Hà Tiên', 'Thị xã'),
(902, 91, 'Huyện Kiên Lương', 'Huyện'),
(903, 91, 'Huyện Hòn Đất', 'Huyện'),
(904, 91, 'Huyện Tân Hiệp', 'Huyện'),
(905, 91, 'Huyện Châu Thành', 'Huyện'),
(906, 91, 'Huyện Giồng Riềng', 'Huyện'),
(907, 91, 'Huyện Gò Quao', 'Huyện'),
(908, 91, 'Huyện An Biên', 'Huyện'),
(909, 91, 'Huyện An Minh', 'Huyện'),
(910, 91, 'Huyện Vĩnh Thuận', 'Huyện'),
(911, 91, 'Huyện Phú Quốc', 'Huyện'),
(912, 91, 'Huyện Kiên Hải', 'Huyện'),
(913, 91, 'Huyện U Minh Thượng', 'Huyện'),
(914, 91, 'Huyện Giang Thành', 'Huyện'),
(916, 92, 'Quận Ninh Kiều', 'Quận'),
(917, 92, 'Quận Ô Môn', 'Quận'),
(918, 92, 'Quận Bình Thuỷ', 'Quận'),
(919, 92, 'Quận Cái Răng', 'Quận'),
(923, 92, 'Quận Thốt Nốt', 'Quận'),
(924, 92, 'Huyện Vĩnh Thạnh', 'Huyện'),
(925, 92, 'Huyện Cờ Đỏ', 'Huyện'),
(926, 92, 'Huyện Phong Điền', 'Huyện'),
(927, 92, 'Huyện Thới Lai', 'Huyện'),
(930, 93, 'Thành phố Vị Thanh', 'Thành phố'),
(931, 93, 'Thị xã Ngã Bảy', 'Thị xã'),
(932, 93, 'Huyện Châu Thành A', 'Huyện'),
(933, 93, 'Huyện Châu Thành', 'Huyện'),
(934, 93, 'Huyện Phụng Hiệp', 'Huyện'),
(935, 93, 'Huyện Vị Thuỷ', 'Huyện'),
(936, 93, 'Huyện Long Mỹ', 'Huyện'),
(937, 93, 'Thị xã Long Mỹ', 'Thị xã'),
(941, 94, 'Thành phố Sóc Trăng', 'Thành phố'),
(942, 94, 'Huyện Châu Thành', 'Huyện'),
(943, 94, 'Huyện Kế Sách', 'Huyện'),
(944, 94, 'Huyện Mỹ Tú', 'Huyện'),
(945, 94, 'Huyện Cù Lao Dung', 'Huyện'),
(946, 94, 'Huyện Long Phú', 'Huyện'),
(947, 94, 'Huyện Mỹ Xuyên', 'Huyện'),
(948, 94, 'Thị xã Ngã Năm', 'Thị xã'),
(949, 94, 'Huyện Thạnh Trị', 'Huyện'),
(950, 94, 'Thị xã Vĩnh Châu', 'Thị xã'),
(951, 94, 'Huyện Trần Đề', 'Huyện'),
(954, 95, 'Thành phố Bạc Liêu', 'Thành phố'),
(956, 95, 'Huyện Hồng Dân', 'Huyện'),
(957, 95, 'Huyện Phước Long', 'Huyện'),
(958, 95, 'Huyện Vĩnh Lợi', 'Huyện'),
(959, 95, 'Thị xã Giá Rai', 'Thị xã'),
(960, 95, 'Huyện Đông Hải', 'Huyện'),
(961, 95, 'Huyện Hoà Bình', 'Huyện'),
(964, 96, 'Thành phố Cà Mau', 'Thành phố'),
(966, 96, 'Huyện U Minh', 'Huyện'),
(967, 96, 'Huyện Thới Bình', 'Huyện'),
(968, 96, 'Huyện Trần Văn Thời', 'Huyện'),
(969, 96, 'Huyện Cái Nước', 'Huyện'),
(970, 96, 'Huyện Đầm Dơi', 'Huyện'),
(971, 96, 'Huyện Năm Căn', 'Huyện'),
(972, 96, 'Huyện Phú Tân', 'Huyện'),
(973, 96, 'Huyện Ngọc Hiển', 'Huyện');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `full_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `tel` int(11) NOT NULL,
  `province_id` int(11) NOT NULL,
  `district_td` int(11) NOT NULL,
  `address` varchar(255) NOT NULL,
  `note` varchar(255) NOT NULL,
  `created_at` date NOT NULL,
  `update_at` date NOT NULL,
  `amount` int(11) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `slug` varchar(50) NOT NULL,
  `img` int(11) NOT NULL,
  `content` text NOT NULL,
  `description` varchar(255) NOT NULL,
  `created_at` text NOT NULL,
  `meta_descripition` int(11) NOT NULL,
  `meta_title` int(11) NOT NULL,
  `meta_key_word` int(11) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `slug` varchar(50) NOT NULL,
  `img` int(11) NOT NULL,
  `link` varchar(255) NOT NULL,
  `desciption` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `position` varchar(255) NOT NULL,
  `created_at` text NOT NULL,
  `post_categorie_id` int(11) NOT NULL,
  `meta_descripition` int(11) NOT NULL,
  `meta_title` int(11) NOT NULL,
  `meta_key_word` int(11) NOT NULL,
  `is_featured` int(11) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `post_categories`
--

CREATE TABLE `post_categories` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `slug` varchar(50) NOT NULL,
  `status` int(11) NOT NULL,
  `parrent_id` int(11) NOT NULL,
  `image` int(11) NOT NULL,
  `description` int(11) NOT NULL,
  `meta_key_word` int(11) NOT NULL,
  `meta_descripition` int(11) NOT NULL,
  `meta_title` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `post_categories`
--

INSERT INTO `post_categories` (`id`, `name`, `slug`, `status`, `parrent_id`, `image`, `description`, `meta_key_word`, `meta_descripition`, `meta_title`) VALUES
(1, 'Converse', 'Converse', 1, 0, 0, 0, 0, 0, 0),
(2, 'Alexander McQueen', 'Alexander McQueen', 1, 0, 0, 0, 0, 0, 0),
(3, 'Balenciaga', 'Balenciaga', 1, 0, 0, 0, 0, 0, 0),
(4, 'Nike', 'Nike', 1, 0, 0, 0, 0, 0, 0),
(5, 'Adidas', 'Adidas', 1, 0, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `product_imgs`
--

CREATE TABLE `product_imgs` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `img` int(11) NOT NULL,
  `is_featured` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `produtcs`
--

CREATE TABLE `produtcs` (
  `id` int(11) NOT NULL,
  `sku` varchar(50) NOT NULL,
  `name` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `price` float NOT NULL,
  `qty` int(11) NOT NULL,
  `brand_id` int(11) NOT NULL,
  `categories_id` int(11) NOT NULL,
  `description` varchar(255) NOT NULL,
  `view` int(11) NOT NULL,
  `content` int(11) NOT NULL,
  `is_new` int(11) NOT NULL DEFAULT '0',
  `is_promo` int(11) NOT NULL,
  `is_featured` int(11) NOT NULL,
  `in_sale` int(11) NOT NULL,
  `created_at` date NOT NULL,
  `update_at` date NOT NULL,
  `rank` float NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `produtcs`
--

INSERT INTO `produtcs` (`id`, `sku`, `name`, `slug`, `price`, `qty`, `brand_id`, `categories_id`, `description`, `view`, `content`, `is_new`, `is_promo`, `is_featured`, `in_sale`, `created_at`, `update_at`, `rank`, `status`) VALUES
(5, 'CV1', 'Giày Converse Chuck Taylor All Star', 'Giày Converse Chuck Taylor All Star', 2000000, 12, 1, 1, 'giày ', 0, 0, 0, 0, 0, 0, '0000-00-00', '0000-00-00', 5, 1),
(6, 'CV2', 'Giày Converse Jack Purcell', 'Giày Converse Jack Purcell', 2500000, 43, 1, 1, 'giày', 0, 0, 0, 0, 0, 0, '0000-00-00', '0000-00-00', 0, 1),
(7, 'CV3', 'Giày Converse One Star', 'Giày Converse One Star', 3000000, 43, 1, 1, '', 0, 0, 0, 0, 0, 0, '0000-00-00', '0000-00-00', 0, 1),
(8, 'MC1', 'ALEXANDER MCQUEEN SOLE SNEAKER', 'ALEXANDER MCQUEEN SOLE SNEAKER', 5000000, 21, 2, 2, '', 0, 0, 0, 0, 0, 0, '0000-00-00', '0000-00-00', 0, 1),
(9, 'bl1', 'Balenciaga speed cổ thấp', 'Balenciaga speed cổ thấp', 5000000, 0, 3, 3, '', 0, 0, 0, 0, 0, 0, '0000-00-00', '0000-00-00', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `provinces`
--

CREATE TABLE `provinces` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `code` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `provinces`
--

INSERT INTO `provinces` (`id`, `name`, `code`) VALUES
(1, 'Thành phố Hà Nội', 1),
(2, 'Tỉnh Hà Giang', 2),
(3, 'Tỉnh Cao Bằng', 4),
(4, 'Tỉnh Bắc Kạn', 6),
(5, 'Tỉnh Tuyên Quang', 8),
(6, 'Tỉnh Lào Cai', 10),
(7, 'Tỉnh Điện Biên', 11),
(8, 'Tỉnh Lai Châu', 12),
(9, 'Tỉnh Sơn La', 14),
(10, 'Tỉnh Yên Bái', 15),
(11, 'Tỉnh Hoà Bình', 17),
(12, 'Tỉnh Thái Nguyên', 19),
(13, 'Tỉnh Lạng Sơn', 20),
(14, 'Tỉnh Quảng Ninh', 22),
(15, 'Tỉnh Bắc Giang', 24),
(16, 'Tỉnh Phú Thọ', 25),
(17, 'Tỉnh Vĩnh Phúc', 26),
(18, 'Tỉnh Bắc Ninh', 27),
(19, 'Tỉnh Hải Dương', 30),
(20, 'Thành phố Hải Phòng', 31),
(21, 'Tỉnh Hưng Yên', 33),
(22, 'Tỉnh Thái Bình', 34),
(23, 'Tỉnh Hà Nam', 35),
(24, 'Tỉnh Nam Định', 36),
(25, 'Tỉnh Ninh Bình', 37),
(26, 'Tỉnh Thanh Hóa', 38),
(27, 'Tỉnh Nghệ An', 40),
(28, 'Tỉnh Hà Tĩnh', 42),
(29, 'Tỉnh Quảng Bình', 44),
(30, 'Tỉnh Quảng Trị', 45),
(31, 'Tỉnh Thừa Thiên Huế', 46),
(32, 'Thành phố Đà Nẵng', 48),
(33, 'Tỉnh Quảng Nam', 49),
(34, 'Tỉnh Quảng Ngãi', 51),
(35, 'Tỉnh Bình Định', 52),
(36, 'Tỉnh Phú Yên', 54),
(37, 'Tỉnh Khánh Hòa', 56),
(38, 'Tỉnh Ninh Thuận', 58),
(39, 'Tỉnh Bình Thuận', 60),
(40, 'Tỉnh Kon Tum', 62),
(41, 'Tỉnh Gia Lai', 64),
(42, 'Tỉnh Đắk Lắk', 66),
(43, 'Tỉnh Đắk Nông', 67),
(44, 'Tỉnh Lâm Đồng', 68),
(45, 'Tỉnh Bình Phước', 70),
(46, 'Tỉnh Tây Ninh', 72),
(47, 'Tỉnh Bình Dương', 74),
(48, 'Tỉnh Đồng Nai', 75),
(49, 'Tỉnh Bà Rịa - Vũng Tàu', 77),
(50, 'Thành phố Hồ Chí Minh', 79),
(51, 'Tỉnh Long An', 80),
(52, 'Tỉnh Tiền Giang', 82),
(53, 'Tỉnh Bến Tre', 83),
(54, 'Tỉnh Trà Vinh', 84),
(55, 'Tỉnh Vĩnh Long', 86),
(56, 'Tỉnh Đồng Tháp', 87),
(57, 'Tỉnh An Giang', 89),
(58, 'Tỉnh Kiên Giang', 91),
(59, 'Thành phố Cần Thơ', 92),
(60, 'Tỉnh Hậu Giang', 93),
(61, 'Tỉnh Sóc Trăng', 94),
(62, 'Tỉnh Bạc Liêu', 95),
(63, 'Tỉnh Cà Mau', 96);

-- --------------------------------------------------------

--
-- Table structure for table `relate_poducts`
--

CREATE TABLE `relate_poducts` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `relate_product_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `reviews`
--

CREATE TABLE `reviews` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` date NOT NULL,
  `update_at` date NOT NULL,
  `content` varchar(255) NOT NULL,
  `rate` int(11) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `brands`
--
ALTER TABLE `brands`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `slug` (`slug`);

--
-- Indexes for table `coupons`
--
ALTER TABLE `coupons`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer_groups`
--
ALTER TABLE `customer_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `districts`
--
ALTER TABLE `districts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `province_id` (`province_code`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `post_categories`
--
ALTER TABLE `post_categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `slug` (`slug`);

--
-- Indexes for table `product_imgs`
--
ALTER TABLE `product_imgs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `produtcs`
--
ALTER TABLE `produtcs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `brand_id` (`brand_id`),
  ADD KEY `categories_id` (`categories_id`);

--
-- Indexes for table `provinces`
--
ALTER TABLE `provinces`
  ADD PRIMARY KEY (`id`),
  ADD KEY `code` (`code`);

--
-- Indexes for table `relate_poducts`
--
ALTER TABLE `relate_poducts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reviews`
--
ALTER TABLE `reviews`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `brands`
--
ALTER TABLE `brands`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `coupons`
--
ALTER TABLE `coupons`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `customer_groups`
--
ALTER TABLE `customer_groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `districts`
--
ALTER TABLE `districts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=974;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `post_categories`
--
ALTER TABLE `post_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `product_imgs`
--
ALTER TABLE `product_imgs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `produtcs`
--
ALTER TABLE `produtcs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `provinces`
--
ALTER TABLE `provinces`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;

--
-- AUTO_INCREMENT for table `relate_poducts`
--
ALTER TABLE `relate_poducts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `reviews`
--
ALTER TABLE `reviews`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `districts`
--
ALTER TABLE `districts`
  ADD CONSTRAINT `districts_ibfk_1` FOREIGN KEY (`province_code`) REFERENCES `provinces` (`code`);

--
-- Constraints for table `produtcs`
--
ALTER TABLE `produtcs`
  ADD CONSTRAINT `produtcs_ibfk_1` FOREIGN KEY (`brand_id`) REFERENCES `brands` (`id`),
  ADD CONSTRAINT `produtcs_ibfk_2` FOREIGN KEY (`categories_id`) REFERENCES `post_categories` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
