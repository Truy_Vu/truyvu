0<?php  
   require('connect.php');
   $err = []; 
     $admin_id = $_GET['id'];
                 $sql = "SELECT * FROM admin WHERE id = '" . $admin_id . "' ";
                 $query = $db->query($sql);
                 $admin = $query->fetch_assoc();
                 if (is_null($admin)) {
                   header("Location: admin_manage.php");
                 }

    if (isset($_POST['sub'])) {
        if (!isset($_POST['username']) || $_POST['username'] == "") {
           $err[] = "Nhập username";
        }
         if (!isset($_POST['fullname']) || $_POST['fullname'] == "") {
           $err[] = "Nhập FullName";
        }
         if (!isset($_POST['email']) || $_POST['email'] == "") {
           $err[] = "Nhập Email";
        }
         if (!isset($_POST['pass']) || $_POST['pass'] == "") {
           $err[] = "Nhập Password";
        }
         if (!isset($_POST['roles']) || $_POST['roles'] == "") {
           $err[] = "Nhập Chức Vụ";
        }

        if (count($err) == 0) {
          $username = $_POST['username'];
          $fullname = $_POST['fullname'];
          $email = $_POST['email'];
          $pass = $_POST['pass'];
          $roles = $_POST['roles'];
          $status = $_POST['status'];
          $sql = "UPDATE admin SET  username = '". $username ."',fullname = '". $fullname ."', email = '". $email ."',pass = '". $pass ."',roles = '". $roles ." ',status = '". $status ."' WHERE id = ". $admin_id ;
          $query = $db->query($sql);

        }
    }

   
?>
<!DOCTYPE html>
<html>
<head>
	<title>Edit Admin</title>

	<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="css/fontawesome-all.min.css">
    <link rel="stylesheet" type="text/css" href="css/owl.carousel.css">
   <link rel="yylesheet" type="text/css" href="css/owl.theme.default.min.css">
  <link rel="stylesheet" type="text/css" href="css/style_trangchu.css">
</head>
<body>
       <center><h1>Thay Đổi Admin</h1></h2>
     </center>
  <br>
  <br>
  <br>
   <center>
      <?php 
          if (isset($_POST['sub'])) {
               if (count($err) > 0) {
                 for ($i=0; $i < count($err); $i++) { ?>
            <p style="color: red">     
                <?php  echo $err[$i] . "<br>"; ?>
            </p>        
           <?php      }
               } else {
                   header("Location: admin_manage.php");
               }           
          }
      ?>
  </center>    
  <br>
  <div class="container">
    <form action="" method="POST">
      <table border="1px" class="text-center">
          <tr>
            <td>
              UserName
            </td>
            <td>
              Tên 
            </td>
            <td>
              Email
            </td>
            <td>
              Pass
            </td>
            <td>
              Status
            </td>
            <td>
              Roles
            </td>
          </tr>
           <tr>
              <td><input type="text" name="username" value="<?php echo $admin['username']; ?>"></td>
              <td><input type="text" name="fullname" value="<?php echo $admin['fullname']; ?>"></td>
              <td><input type="text" name="email" value="<?php echo $admin['email']; ?>"></td>
              <td><input type="text" name="pass" value="<?php echo $admin['pass']; ?>"></td>
              <td>
                  <p>
                        <input type="radio" name="status" value="1" <?php if ($admin['status'] == 1) {
                           echo "checked";
                        } ?>>
                        Đang hoạt động
                  </p>
                  <p>
                        <input type="radio" name="status" value="0" <?php if ($admin['status'] != 1) {
                           echo "checked";
                        } ?>>
                        Không hoạt động
                  </p> 
              </td>
              <td><input type="text" name="roles" value="<?php echo $admin['roles']; ?>"></td>
            </tr>    
      </table>
      <br>
      <br>
       <center><input type="submit" name="sub" value="OK"></center>
     </div>
  </form>   
    <script src="js/jquery-3.2.1.min.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script> 
</body>
</html>