<?php  
   require('connect.php');
   $err = []; 
   
?>
<!DOCTYPE html>
<html>
<head>
	<title>Manage brands</title>

	<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="css/fontawesome-all.min.css">
    <link rel="stylesheet" type="text/css" href="css/owl.carousel.css">
   <link rel="yylesheet" type="text/css" href="css/owl.theme.default.min.css">
  <link rel="stylesheet" type="text/css" href="css/style_trangchu.css">
</head>
<body>
     <center>
     	<h1>Quản Lí Nhan</h1>
     </center>
	<br>
	<br>
	<br>

	<br>
	<div class="container">
      <a href="products_add.php">Thêm sản phẩm</a>
      <br>
      <br>
      <table border="1px" class="text-center">
      	  <tr>
      	  	<td>
      	  		Name
      	  	</td>
      	  	<td>
      	  		Slug
      	  	</td>
      	  	<td>
      	  		Logo
      	  	</td>
      	  	<td>
      	  		Description
      	  	</td>
      	  	<td>
      	  		Status
      	  	</td>
      	  	<td>Edit</td>
      	  </tr>
           <?php 
              
                $sql = "SELECT * FROM brands";         
                $query = $db->query($sql);
                $result = $query->fetch_all(MYSQLI_ASSOC);
                foreach ($result as $brand) { ?>
            <tr>
            	<td><?php echo $brand['name']; ?></td>
            	<td><?php echo $brand['slug']; ?></td>
            	<td><?php echo $brand['logo']; ?></td>
                <td><?php echo $brand['description']; ?></td>
                <td><?php echo $brand['status']; ?></td>
            	</td>
                <td>
                	  <p><a href="brands_edit.php?id=<?php echo $brand['id'] ?>">Cập nhật thông tin</a></p>
                     <p><a href="brands_delete.php?id=<?php echo $brand['id'] ?>">Hủy</a></p>

                </td>
            </tr>    
            <?php    
                }
            ?>
      </table>
     </div>
    <script src="js/jquery-3.2.1.min.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script>	
</body>
</html>