<?php  
   require('connect.php');
   $err = []; 
     $product_id = $_GET['id'];
                 $sql = "SELECT * FROM products WHERE id = '" . $product_id . "' ";
                 $query = $db->query($sql);
                 $product = $query->fetch_assoc();
                 if (is_null($product_id)) {
                   header("Location: products_manage.php");
                 }

    if (isset($_POST['sub'])) {
        if (!isset($_POST['edit']) || $_POST['edit'] == "") {
           $err[] = "Chọn sự thay đổi (Tăng/Giảm)";
        }
        if (!isset($_POST['num']) || $_POST['num'] == "") {
           $err[] = "Nhập số lượng";
        } else {
           $num = $_POST['num'];
           if ($num > $product['qty'] && $_POST['edit'] == 0) {
               $err[] = "Số lượng quá lớn";
           }
        }


        if (count($err) == 0) {
          $name = $_POST['name'];
          $name = $_POST['brand_id'];
          $name = $_POST['price'];
          if ($_POST['edit'] == 1) {
               $new = $product['qty'] + $num; 
          } else {
               $new = $product['qty'] - $num;            
          } 
          $sql = "UPDATE products SET  name = '". $name ."',brand_id = '". $brand_id ."', price = '". $price ."',qty = '". $new ."' WHERE id = '". $product_id ."'";
          $query = $db->query($sql);

        }
    }

   
?>
<!DOCTYPE html>
<html>
<head>
	<title>Edit Products</title>

	<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="css/fontawesome-all.min.css">
    <link rel="stylesheet" type="text/css" href="css/owl.carousel.css">
   <link rel="yylesheet" type="text/css" href="css/owl.theme.default.min.css">
  <link rel="stylesheet" type="text/css" href="css/style_trangchu.css">
</head>
<body>
       <center><h1>Thay Đổi Sản Phẩm</h1></h2>
     </center>
  <br>
  <br>
  <br>
   <center>
      <?php 
          if (isset($_POST['sub'])) {
               if (count($err) > 0) {
                 for ($i=0; $i < count($err); $i++) { ?>
            <p style="color: red">     
                <?php  echo $err[$i] . "<br>"; ?>
            </p>        
           <?php      }
               } else {
                   header("Location: products_manage.php");
               }           
          }
      ?>
  </center>    
  <br>
  <div class="container">
    <form action="" method="POST">
      <table border="1px" class="text-center">
          <tr>
            <td>
              Hình ảnh
            </td>
            <td>
              Tên sản phẩm
            </td>
            <td>
              Nhãn hiệu
            </td>
            <td>
              Đơn giá
            </td>
            <td>
              Số lượng còn lại
            </td>
            <td>
              Đánh giá
            </td>
            <td>
              Trạng thái
            </td>
            <td>Edit</td>
          </tr>
           <tr>

              <td><?php?></td>
              <td><input type="text" name="name" value="<?php echo $product['name']; ?>"></td>
              <td>
               <select name="brand_id">
                <?php 
                      $sql = "SELECT * FROM brands";
                      $query = $db->query($sql);
                      $result = $query->fetch_all(MYSQLI_ASSOC); 
                     foreach ($result as $brand ) { ?>
                <option value="<?php echo($brand['id']) ?>" <?php if ($product['brand_id'] == $brand['id']) echo "selected"; {
                  # code...
                } ?>> <?php echo $brand['name']; ?> </option>
             <?php           
                             }
              ?>
               </select>
            </td>
              <td><input type="text" name="price" value="<?php echo $product['price']; ?>"></td>
              <td><input type="text" name="qty" value="<?php echo $product['qty']; ?>"></td>
              <td><?php echo $product['rank']; ?></td>
              <td><?php  if ($product['status'] == 1) {
                    echo "Đang treo";
              } ?>    
              </td>
              </td>
                <td>
                  <p>
                     <input type="radio" name="edit" value="1" <?php 
                             if (isset($_POST['sub'])) {
                                 if (isset($_POST['edit']) && $_POST['edit'] == 1) {
                                    echo "checked";
                                 }
                             }
                      ?>> Thêm
                  </p>
                  <p>
                     <input type="radio" name="edit" value="0" <?php 
                             if (isset($_POST['sub'])) {
                                 if (isset($_POST['edit']) && $_POST['edit'] != 1) {
                                    echo "checked";
                                 }
                             }
                      ?>> Bớt
                  </p>
                  <p>
                    Số Lượng <input type="number" name="num" value="<?php if(isset($_POST['sub'])) echo $num; ?>">
                  </p>
                </td>
            </tr>    
            </tr>
      </table>
      <br>
      <br>
       <center><input type="submit" name="sub" value="OK"></center>
     </div>
  </form>   
    <script src="js/jquery-3.2.1.min.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script> 
</body>
</html>