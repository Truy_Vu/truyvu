<?php  
   $err = [];
   session_start();
   require('connect.php');
   if (isset($_POST['sub'])) {
         if (!isset($_POST['name']) || $_POST['name'] == "") {
             $err[] = "Nhập tên";
         }
          if (!isset($_POST['slug']) || $_POST['slug'] == "") {
             $err[] = "Nhập Slug";
         }
          if (!isset($_POST['price']) || $_POST['price'] == "") {
             $err[] = "Nhập giá";
         }
          if (!isset($_POST['qty']) || $_POST['qty'] == "") {
             $err[] = "Nhập số lượng";
         }
          if (!isset($_POST['brand_id']) || $_POST['brand_id'] == "") {
             $err[] = "Nhập nhãn hiệu";
         }
          if (!isset($_POST['post_categorie']) || $_POST['post_categorie'] == "") {
             $err[] = "Nhập danh mục";
         }
          if (!isset($_POST['description']) || $_POST['description'] == "") {
             $err[] = "Nhập môt tả";
         }
       if (count($err) == 0) {
            $name = $_POST['name'];
            $slug = $_POST['slug'];
            $price = $_POST['price'];
            $qty = $_POST['qty'];
            $brand_id = $_POST['brand_id'];
            $post_categorie = $_POST['post_categorie'];
            $description = $_POST['description'];
            $sql = "INSERT INTO products (`name`, `slug`, `price`, `qty`, `brand_id`,`categories_id`, `description`) VALUES ('". $name ."','". $slug ."',". $price .",". $qty .",". $brand_id .",". $post_categorie .",'". $description ."')";
            echo $sql;
            $query = $db->query($sql);
       }
   }
   
?>
<!DOCTYPE html>
<html>
<head>
	<title>Edit Products</title>

	<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="css/fontawesome-all.min.css">
    <link rel="stylesheet" type="text/css" href="css/owl.carousel.css">
   <link rel="yylesheet" type="text/css" href="css/owl.theme.default.min.css">
  <link rel="stylesheet" type="text/css" href="css/style_trangchu.css">
</head>
<body>
   <center>
   	<h1>
   		Thêm sản phẩm
   	</h1>
   </center>  
    <?php 
           if (isset($_POST['sub'])) {

                   if (count($err) > 0) {
                       for ($i=0; $i < count($err); $i++) { 
                           echo  $err[$i] . "<br>";
                       }
                   }

                   if (count($err) == 0) {
                       header("Location: products_manage.php");
                   }
                      }        
     ?>
    <form method="POST" action="">
    	 <table border="1px">
    	 	<tr>
    	 		 <td>
    	 		 	Tên Sản Phẩm
    	 		 </td>
    	 		 <td>
    	 		 	Slug
    	 		 </td>
    	 		 <td>
    	 		 	 Giá
    	 		 </td>
    	 		 <td>
    	 		 	 Số Lượng Trong Kho
    	 		 </td>
    	 		 <td>
    	 		 	Thương Hiệu
    	 		 </td>
    	 		 <td>
    	 		 	Danh Mục
    	 		 </td>
    	 		 <td>
    	 		 	Mô tả
    	 		 </td>
       </tr>
       <tr>
           <td>
               <input type="text" name="name" value="<?php  if(isset($_POST['sub'])){
                            if(isset($_POST['name'])) {
                                 echo $_POST['name'];
                            }
               } ?>" >
           </td>
          
       <td>
               <input type="text" name="slug" value="<?php  if(isset($_POST['sub'])){
                            if(isset($_POST['slug'])) {
                                 echo $_POST['slug'];
                            }
               } ?>" >
       </td>
       <td>
               <input type="text" name="price" value="<?php  if(isset($_POST['sub'])){
                            if(isset($_POST['price'])) {
                                 echo $_POST['price'];
                            }
               } ?>" >
       </td>
       <td>
               <input type="text" name="qty" value="<?php  if(isset($_POST['sub'])){
                            if(isset($_POST['qty'])) {
                                 echo $_POST['qty'];
                            }
               } ?>" >
       </td>  
       <td>
          <select name="brand_id">
              <option value="">-Chọn-</option>
            <?php   
                $sql = "SELECT * FROM brands";
                $query = $db->query($sql);
                $result = $query->fetch_all(MYSQLI_ASSOC);
                foreach ($result as $brand) {?>
               <option value="<?php   echo $brand['id'] ?>"  <?php  if(isset($_POST['sub'])) {
                   if(isset($_POST['sub']) && $_POST['brand_id'] == $brand['id']) {
                       echo "selected";
                   }
               } ?>> <?php  echo $brand['name'];  ?></option>      
            <?php    }
            ?>
          </select>
       </td> 
       <td>
         <select name="post_categorie">
              <option value="">-Chọn-</option>
            <?php   
                $sql = "SELECT * FROM post_categories";
                $query = $db->query($sql);
                $result = $query->fetch_all(MYSQLI_ASSOC);
                foreach ($result as $post_categories) {?>
               <option value="<?php   echo $post_categories['id'] ?>"  <?php  if(isset($_POST['sub'])) {
                   if(isset($_POST['sub']) && $_POST['post_categorie'] == $post_categories['id']) {
                       echo "selected";
                   }
               } ?>> <?php  echo $post_categories['name'];  ?></option>      
            <?php    }
            ?>
          </select>
       </td>    
       <td>
               <input type="text" name="description" value="<?php  if(isset($_POST['sub'])){
                            if(isset($_POST['description'])) {
                                 echo $_POST['description'];
                            }
               } ?>" >
       </td> 
       </tr> 
    	 </table>
      <p>
        <input type="submit" name="sub">
      </p>
    </form>
    <script src="js/jquery-3.2.1.min.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script> 
</body>
</html>