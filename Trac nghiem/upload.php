<?php  
          session_start();
          if (!isset($_SESSION['user'])) {
              header('Location: dang_nhap.php');
          }
          $targetDir = 'uploads/';
          $allow = ['png', 'jpg', 'gif', 'bmp','tiff'];
          $err = [];
          if (isset($_POST['submit'])) {
          	 $targetFile = $targetDir . $_FILES['file']['name'];
          	 $FileType = @end(explode('.', $_FILES['file']['name']));
              
             if (!in_array($FileType, $allow)) {
             	 $err[] = "<center> <h3 style = 'color :red'>Please Upload:  Image File (png, jpg, gif, bmp and tiff) For Your Avatar</h3> </center>";
             }
             if (count($err) != 0) {
             	  for ($i=0; $i < count($err) ; $i++) { 
             	  	echo $err[$i] . "<br>";
             	  }
             }
             else {

             	  if (move_uploaded_file($_FILES['file']['tmp_name'], $targetFile)) {
             	  	  $_SESSION['avatar'] = ['link' => $targetFile, 'nick' => $_POST['nickname']];
             	  	  header('Location: tracnghiem.php');
             	  	   
             	  }
             }

          }
?>
<!DOCTYPE html>
<html>
<head>
	<title>Upload</title>
   <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="css/fontawesome-all.min.css">
    <link rel="stylesheet" type="text/css" href="css/owl.carousel.css">
   <link rel="yylesheet" type="text/css" href="css/owl.theme.default.min.css">
  <link rel="stylesheet" type="text/css" href="style_upload.css">
</head>
<body>
	<div class="all">
    <div class="container">
     <div class="box">
        <h4>Update Your Avatar</h4>
     <form action="" method="POST" enctype="multipart/form-data">
          
              
            
             <p>
              Upload Avatar(*) : 
              <input type="file" name="file" id="file">
             </p>
             
             <div class="sub">
               <p>
               <input type="submit" value=" Submit" name="submit">
             </p>
             </div>
     </div>
     </form> 
    </div>
  </div>

  <script src="js/jquery-3.2.1.min.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
</body>
</html>